using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter{
    public class BitmapFont {
        string alphabet = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        Texture2D texture;

        public BitmapFont(String fontName, Game1 game) {
            texture = game.Content.Load<Texture2D>("Fonts/" + fontName);
        }

        public Vector2 MeasureString(String text, int size) {
            return new Vector2(text.Length * size, size);
        }

        public void DrawString(String text, Rectangle rect, Color color, SpriteBatch spriteBatch) {
            int x = 0;
            int y = rect.Y;
            foreach (Char character in text) {
                int offset = alphabet.IndexOf(character);
                if (character == '\n') {
                    x = 0;
                    y += rect.Height;
                    continue;
                }
                else if (character != ' ') 
                {
                    spriteBatch.Draw(texture, new Rectangle(rect.X + x, y, rect.Width, rect.Height), new Rectangle((offset * 8 + (1 + offset)), 1, 8, 8), 
                                                        color, 0f, Vector2.Zero, SpriteEffects.None, 0f);
                }
                x += rect.Width;
            }
        }
    }
}