﻿using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Shooter
{
	public class Level
	{
		Game1 game;
	 	Player player;
		public Player Player
		{
			get
			{
				return player;
			}
		}

		public List<Enemy> EnemyList;
		public List<Bullet> BulletList;
		public List<Explosion> ExplosionList;
		public List<Item> ItemList;
		public List<Tile> TileList;
		public List<Checkpoint> CheckpointList;
		public List<WarningPoint> WarningPointList;
		public List<ScoreText> ScoreTextList;
		public Bomb Bomb;
		public Boss Boss;

		double startTimer;
		const double START_INTERVAL = 2000f;

		double deathTimer;
		const double DEATH_INTERVAL = 1200f;
		bool deathTimerRunning;

		double gameOverTimer;
		const double GAME_OVER_INTERVAL = 3000f;

		double completionTimer;
		const double COMPLETION_INTERVAL = 5000f;
		bool completionTimerRunning;

		double bossDeathTimer;
		const double BOSS_DEATH_INTERVAL = 1500f;
		bool bossDeathTimerRunning;

		double deathPauseTimer;
		const double DEATH_PAUSE_INTERVAL = 500f;

		double bossPauseTimer;
		const double BOSS_PAUSE_INTERVAL = 500f;

		Texture2D tileSet;

		LevelState state;
		public LevelState State
		{
			get
			{
				return state;
			}
		}

		SpriteFont hudFont;

		Hud hud;
		public Hud Hud
		{
			get
			{
				return hud;
			}
		}

		public Song bgm;
		public Song bossBGM;

		protected SoundEffect victorySound;

		BossHealthBar bossHealthBar;
		public BossHealthBar BossHealthBar
		{
			get
			{
				return bossHealthBar;
			}
		}

		Texture2D crosshair;

		string fileName;

		int height;
		int heightInt;

		public Game1 Game
		{
			get
			{
				return game;
			}
		}

		Camera camera;
		public Camera Camera
		{
			get
			{
				return camera;
			}
		}
		SoundEffect pauseSound;

		Checkpoint lastCheckpoint;
		public Checkpoint LastCheckPoint {
			get {
				return lastCheckpoint;
			}
			set {
				lastCheckpoint = value;
			}
		}

		public bool BossPauseRunning {
			get {
				return (bossPauseTimer > 0);
			}
		}

		public bool DeathPauseRunning {
			get {
				return (deathPauseTimer > 0);
			}
		}

		public Level(string fileName, Game1 game)
		{
			this.fileName = fileName;
			this.game = game;
			hudFont = game.Content.Load<SpriteFont>("Fonts/Font");
			hud = new Hud(game);

			bgm = game.Content.Load<Song>("Music/encounter");
			bossBGM = game.Content.Load<Song>("Music/Duel");

			victorySound = game.Content.Load<SoundEffect>("Sounds/Victory");

			crosshair = game.Content.Load<Texture2D>("Graphics/Crosshair");
			pauseSound = game.Content.Load<SoundEffect>("Sounds/Pause");

			deathPauseTimer = 0;
			bossPauseTimer = 0;

			//Initialize();
		}

		/// <summary>
		/// Initializes the level (allows reinitialization)
		/// </summary>
		public void Initialize()
		{
			state = LevelState.Normal;
			deathTimerRunning = false;
			deathTimer = DEATH_INTERVAL;

			gameOverTimer = GAME_OVER_INTERVAL;

			startTimer = START_INTERVAL;
			completionTimer = COMPLETION_INTERVAL;
			completionTimerRunning = false;

			bossDeathTimer = BOSS_DEATH_INTERVAL;
			bossDeathTimerRunning = false;

			hud.SetCenterText("Level " + (game.LevelNo + 1) + " Start");

			Build();

			bossHealthBar = new BossHealthBar(this);

			MediaPlayer.Play(bgm);
		}

		/// <summary>
		/// Build the level base off of the XML file
		/// </summary>
		void Build()
		{
			// Load XML document
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(fileName);

			/*
			// Calculate the height of the level in tiles and integers
			height = Convert.ToInt32(xmlDoc.GetElementsByTagName("Height")[0].InnerText);
			heightInt = height * Globals.TILE_SIZE;
			*/

			// Load BGM
			bgm = game.Content.Load<Song>("Music/" + xmlDoc.GetElementsByTagName("Music")[0].InnerText);

			// Load Tileset
			tileSet = game.Content.Load<Texture2D>("Graphics/" + xmlDoc.GetElementsByTagName("TileSet")[0].InnerText);

			// Initialize object lists
			EnemyList = new List<Enemy>();
			BulletList = new List<Bullet>();
			ExplosionList = new List<Explosion>();
			ItemList = new List<Item>();
			TileList = new List<Tile>();
			CheckpointList = new List<Checkpoint>();
			WarningPointList = new List<WarningPoint>();
			ScoreTextList = new List<ScoreText>();
			
			Bomb = null;

			// Build tiles
			String[] tileMap = xmlDoc.GetElementsByTagName("TileMap")[0].InnerText.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
			int x, y;
			x = y = 0;
			foreach (String line in tileMap)
			{
				String row = line.Replace("\t", ""); // Filter out the tabs in the inner text of the XML node
				foreach (Char tile in row)
				{
					switch (tile)
					{
						case '#':
							TileList.Add(new Wall(x, y, tileSet, this));
							break;
						case '.':
							TileList.Add(new Tile(x, y, false, new Rectangle[] { new Rectangle(1, 1, 16, 16) }, tileSet, this));
							break;
						case ',':
							TileList.Add(new Tile(x, y, false, new Rectangle[] { new Rectangle(41, 20, 16, 16) }, tileSet, this));
							break;
						case '*':
							TileList.Add(new Tile(x, y, false, new Rectangle[] { new Rectangle(61, 1, 16, 16), new Rectangle(1, 20, 16, 16), 
							new Rectangle(81, 1, 16, 16) }, tileSet, this));
							break;
						case '-':
							TileList.Add(new Tile(x, y, false, new Rectangle[] { new Rectangle(21, 1, 16, 16) }, tileSet, this));
							break;
						case '^':
							TileList.Add(new Tile(x, y, false, new Rectangle[] { new Rectangle(41, 1, 16, 16) }, tileSet, this));
							break;
						case 'q':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(61, 20, 16, 16)}, tileSet, this));
							break;
						case 'w':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(81, 20, 16, 16)}, tileSet, this));
							break;
						case 'e':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(1, 39, 16, 16)}, tileSet, this));
							break;
						case 'a':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(21, 39, 16, 16)}, tileSet, this));
							break;
						case 'd':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(41, 39, 16, 16)}, tileSet, this));
							break;
						case 'z':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(61, 39, 16, 16)}, tileSet, this));
							break;
						case 'x':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(81, 39, 16, 16)}, tileSet, this));
							break;
						case 'c':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(1, 58, 16, 16)}, tileSet, this));
							break;
						case 'i':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(21, 58, 16, 16)}, tileSet, this));
							break;
						case 'o':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(41, 58, 16, 16)}, tileSet, this));
							break;
						case 'k':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(61, 58, 16, 16)}, tileSet, this));
							break;
						case 'l':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(81, 58, 16, 16)}, tileSet, this));
							break;
						case '7':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(1, 77, 16, 16)}, tileSet, this));
							break;
						case '8':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(81, 77, 16, 16)}, tileSet, this));
							break;
						case '9':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(21, 77, 16, 16)}, tileSet, this));
							break;
						case '4':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(1, 96, 16, 16)}, tileSet, this));
							break;
						case '6':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(21, 96, 16, 16)}, tileSet, this));
							break;
						case '1':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(41, 77, 16, 16)}, tileSet, this));
							break;
						case '2':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(41, 96, 16, 16)}, tileSet, this));
							break;
						case '3':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(61, 77, 16, 16)}, tileSet, this));
							break;
						case 'Q':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(61, 96, 16, 16)}, tileSet, this));
							break;
						case 'W':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(81, 96, 16, 16)}, tileSet, this));
							break;
						case 'E':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(1, 115, 16, 16)}, tileSet, this));
							break;
						case 'A':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(21, 115, 16, 16)}, tileSet, this));
							break;
						case 'D':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(41, 115, 16, 16)}, tileSet, this));
							break;
						case 'Z':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(61, 115, 16, 16)}, tileSet, this));
							break;
						case 'X':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(81, 115, 16, 16)}, tileSet, this));
							break;
						case 'C':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(1, 134, 16, 16)}, tileSet, this));
							break;
						case 'U':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(21, 134, 16, 16)}, tileSet, this));
							break;
						case 'I':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(41, 134, 16, 16)}, tileSet, this));
							break;
						case 'J':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(61, 134, 16, 16)}, tileSet, this));
							break;
						case 'K':
							TileList.Add(new Tile(x, y, false, new Rectangle[] {new Rectangle(81, 134, 16, 16)}, tileSet, this));
							break;
					}
					x += Globals.TILE_SIZE;
				}
				x = 0;
				y += Globals.TILE_SIZE;
			}

			height = tileMap.Length - 1;
			heightInt = height * Globals.TILE_SIZE;

			// Initialize camera
			camera = new Camera(0, heightInt - Globals.SCREEN_HEIGHT, this);
			//camera = new Camera(0, Globals.SCREEN_HEIGHT, this);

			// Initialize player
			player = new Player((Globals.SCREEN_WIDTH / 2) - (Globals.TILE_SIZE / 2), camera.Rect.Bottom - Globals.TILE_SIZE, this);

			// Initialize boss
			switch (Convert.ToInt32(xmlDoc.GetElementsByTagName("Boss")[0].InnerText))
			{
				case 1:
					Boss = new Boss1((Globals.SCREEN_WIDTH / 2) - 60, -99, this);
					break;
				case 2:
					Boss = new Boss2((Globals.SCREEN_WIDTH / 2) - 119, -99, this);
					break;
				case 3:
					Boss = new Boss3((Globals.SCREEN_WIDTH / 2) - 60, 0, this);
					break;
				case 4:
					Boss = new Boss4((Globals.SCREEN_WIDTH / 2) - 60, -99, this);
					break;
				case 5:
					Boss = new Boss5((Globals.SCREEN_WIDTH / 2) - 32, 0, this);
					break;
				case 6:
					Boss = new Boss6((Globals.SCREEN_WIDTH / 2) - 32, 0, this);
					break;
				case 7:
					Boss = new Boss7(0, 0, this);
					break;
				case 8:
					Boss = new Boss8(0, 64, this);
					break;
			}

			// Create objects
			foreach (XmlNode node in xmlDoc.GetElementsByTagName("Objects")[0].ChildNodes)
			{
				switch (node.Name)
				{
					case "Enemy1":
						EnemyList.Add(new Enemy1(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), 
												(DropItem)Convert.ToInt32(node.Attributes.GetNamedItem("dropItem").Value), this));
						break;
					case "Enemy2":
						EnemyList.Add(new Enemy2(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), 
												(DropItem)Convert.ToInt32(node.Attributes.GetNamedItem("dropItem").Value), this));
						break;
					case "Enemy3":
						EnemyList.Add(new Enemy3(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), 
						                        (DropItem)Convert.ToInt32(node.Attributes.GetNamedItem("dropItem").Value), this));
						break;
					case "Enemy4":
						EnemyList.Add(new Enemy4(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
												this));
						break;
					case "Enemy5":
						EnemyList.Add(new Enemy5(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), 
												(DropItem)Convert.ToInt32(node.Attributes.GetNamedItem("dropItem").Value), this));
						break;
					case "Enemy6":
						Enemy6.SpawnGroup(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
						                this);
						break; 
					case "Enemy7":

						EnemyList.Add(new Enemy7(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), 
						                this));
						break;
					case "Enemy8":
						Enemy8.SpawnGroup(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
						                this);
						break;
					case "Enemy9":
						/*
						EnemyList.Add(new Enemy9(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), 0, 
												(DropItem)Convert.ToInt32(node.Attributes.GetNamedItem("dropItem").Value), this));
						*/
						Enemy9.SpawnGroup(convertToMapCoords(node.Attributes.GetNamedItem("y").Value), Convert.ToInt32(node.Attributes.GetNamedItem("side").Value), this);
						break;
					case "Enemy10":
						EnemyList.Add(new Enemy10(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), 
												(DropItem)Convert.ToInt32(node.Attributes.GetNamedItem("dropItem").Value), this));
						break;
					case "Asteroid":
						EnemyList.Add(new Asteroid(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
						                (DropItem)Convert.ToInt32(node.Attributes.GetNamedItem("dropItem").Value), this));
						break;
					case "Turret":
						EnemyList.Add(new Turret(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
										this));
						break;
					case "Tank":
						EnemyList.Add(new Tank(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
											(Direction)Convert.ToInt32(node.Attributes.GetNamedItem("facing").Value), this));
						break;
					case "Boat":
						EnemyList.Add(new Boat(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
											(Direction)Convert.ToInt32(node.Attributes.GetNamedItem("facing").Value), this));
						break;
					case "Barrier":
						EnemyList.Add(new Barrier(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value),
											Convert.ToInt32(node.Attributes.GetNamedItem("length").Value), this));
						break;
					case "Checkpoint":
						CheckpointList.Add(new Checkpoint(convertToMapCoords(node.Attributes.GetNamedItem("x").Value), convertToMapCoords(node.Attributes.GetNamedItem("y").Value), this));
						break;
					case "WarningPoint":
						WarningPointList.Add(new WarningPoint(convertToMapCoords(node.Attributes.GetNamedItem("y").Value)));
						break;
				}
			}

		}

		public void StartDeathTimer()
		{
			deathTimerRunning = true;
		}

		public void StartBossDeathTimer() {
			bossDeathTimerRunning = true;
		}

		public void StartDeathPause() {
			MediaPlayer.Stop();
			deathPauseTimer = DEATH_PAUSE_INTERVAL;
		}

		public void StartBossPause() {
			MediaPlayer.Stop();
			bossPauseTimer = BOSS_PAUSE_INTERVAL;
		}

		/// <summary>
		/// Sets the level to completion state.
		/// </summary>
		public void End()
		{
			victorySound.Play();
			state = LevelState.Completed;
			completionTimerRunning = true;
			hud.SetCenterText("Level " + (game.LevelNo + 1) + " Completed");
		}

		/// <summary>
		/// Reset this level.
		/// </summary>
		public void Reset()
		{
			Initialize();
		}

		public void ReturnToLastCheckpoint() {
			//MediaPlayer.Stop();
			//player.IsDead = false;
			Reset();
			player.SetPosition(lastCheckpoint.Rect.X, lastCheckpoint.Rect.Y);
			camera.SetPosition(0, lastCheckpoint.Rect.Y - Globals.SCREEN_HEIGHT - 32);
			//MediaPlayer.Play(bgm);
		}

		protected int convertToMapCoords(string x) {
			return Convert.ToInt32(x) * Globals.TILE_SIZE;
		}

		public void ShowBossHealthBar()
		{
			bossHealthBar.Visible = true;
		}


		/// <summary>
		/// Updates the objects
		/// </summary>
		/// <param name="gameTime">Game time.</param>
		/// <param name="keyState">Current keyboard state</param>
		/// <param name="prevState">Keyboard state of the previous frame</param>
		public void Update(GameTime gameTime, KeyboardState keyState, KeyboardState prevState, GamePadState gamePadState, GamePadState prevPadState)
		{
			hud.Update(gameTime);
			player.Velocity = Vector2.Zero;
			if (deathPauseTimer <= 0 && bossPauseTimer <= 0) {
				switch (state)
				{
				
					case LevelState.Normal:
					if (!player.IsDead)
						{
						player.HandleEvents(keyState, prevState, gamePadState, prevPadState);
						}
						if ((keyState.IsKeyDown(Keys.Enter) && !prevState.IsKeyDown(Keys.Enter)) || 
							(gamePadState.IsButtonDown(Buttons.Start) && !prevPadState.IsButtonDown(Buttons.Start)))
						{
							pauseSound.Play();
							MediaPlayer.Pause();
							game.Mode = Mode.Pause;
						}
						break;
					case LevelState.Dead:
						gameOverTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
						if ((keyState.IsKeyDown(Keys.Z) && !prevState.IsKeyDown(Keys.Z)) || (gameOverTimer <= 0))
						{
							if (Globals.Score > game.HighScoreManager.LowestHighScore) {
								game.ScoreBoard.NameEntry.Enabled = true;
							}
							MediaPlayer.Stop();
							game.Mode = Mode.HighScore;
						}
						break;
					case LevelState.Completed:
						if (completionTimer <= COMPLETION_INTERVAL / 2)
						{
							if (!player.HasTrail) {
								player.HasTrail = true;
								player.TrailStart = new Point(player.Rect.X, player.Rect.Bottom);
							}
							player.Velocity = new Vector2(0, -8f); // fly off-screen
						}
						break;
				}

				// Update game objects
				player.Update(gameTime);
				for (int i = TileList.Count - 1; i >= 0; i--) {
					TileList[i].Update(gameTime);
				}
				for (int i = BulletList.Count - 1; i >= 0; i--)
				{
					BulletList[i].Update(gameTime);
				}
				if (this.Bomb != null)
				{
					this.Bomb.Update(gameTime);
				}
				for (int i = EnemyList.Count - 1; i >= 0; i--)
				{
					if (EnemyList[i].Rect.Bottom > Camera.Rect.Top && EnemyList[i].Rect.Top < Camera.Rect.Bottom + 64)
					{
						EnemyList[i].Update(gameTime);
					}
				}
				for (int i = ExplosionList.Count - 1; i >= 0; i--)
				{
					ExplosionList[i].Update(gameTime);
				}
				for (int i = ScoreTextList.Count - 1; i >= 0; i--) {
					ScoreTextList[i].Update(gameTime);
				}
				if (this.Boss != null)
				{
					if (camera.Rect.Y <= 0)
					{
						this.Boss.Update(gameTime);
					}
				}

				// Scroll the camera
				camera.Scroll();

				if (startTimer >= 0)
				{
					startTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				}

				// Run death timer if running
				if (deathTimerRunning)
				{
					deathTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (deathTimer <= 0)
					{
						if (Globals.Lives > 0)
						{
							/*
							// Reset the player's position
							player.ResetPosition();
							*/
							if (lastCheckpoint != null) {
								ReturnToLastCheckpoint();
							}
							else {
								Reset();
							}

							// Stop the death timer
							deathTimerRunning = false;
							deathTimer = DEATH_INTERVAL;
						}
						else
						{
							state = LevelState.Dead;
							deathTimerRunning = false;
							hud.SetCenterText("Game Over");
						}

					}
				}

				// See if lives is above 10
				if (Globals.Lives > 10) {
					Globals.Lives = 10;
				}

				// Run completion timer if running
				if (completionTimerRunning)
				{
					completionTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (completionTimer <= 0)
					{
						game.NextLevel();
					}
				}
				else if (bossDeathTimerRunning) {
					bossDeathTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (bossDeathTimer <= 0) {
						End();
					}
				}
			}
			else if (deathPauseTimer > 0) {
				deathPauseTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				if (deathPauseTimer <= 0) {
					player.Die();
				}
			}
			else {
				bossPauseTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			}
		}


		/// <summary>
		/// Draw the game objects
		/// </summary>
		/// <param name="spriteBatch">Sprite batch.</param>
		/// <param name="graphicsDevice">Graphics device.</param>
		public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
		{
			graphicsDevice.Clear(Color.Black);

			// Draw the game objects
			spriteBatch.Begin(samplerState: SamplerState.PointClamp, transformMatrix: camera.TransformMatrix);
			if (deathPauseTimer > 0) {
				graphicsDevice.Clear(Color.Red);
			}
			else if (bossPauseTimer > 0) {
				graphicsDevice.Clear(Color.Blue);
			}
			else {
				for (int i = TileList.Count - 1; i >= 0; i--)
				{
					TileList[i].Draw(spriteBatch);
				}
			}
			if (this.Bomb != null)
			{
				this.Bomb.Draw(spriteBatch);
			}
			if (this.Boss != null)
			{
				this.Boss.Draw(spriteBatch);
			}
			for (int i = EnemyList.Count - 1; i >= 0; i--)
			{
				EnemyList[i].Draw(spriteBatch);
			}
			for (int i = ItemList.Count - 1; i >= 0; i--)
			{
				ItemList[i].Draw(spriteBatch);
			}
			for (int i = ExplosionList.Count - 1; i >= 0; i--)
			{
				ExplosionList[i].Draw(spriteBatch);
			}
			player.Draw(spriteBatch);

			for (int i = BulletList.Count - 1; i >= 0; i--)
			{
				BulletList[i].Draw(spriteBatch);
			}
			for (int i = ScoreTextList.Count - 1; i >= 0; i--) {
				ScoreTextList[i].Draw(spriteBatch);
			}

			if (!player.IsDead && state == LevelState.Normal) // Draw crosshairs only when player is alive and the level has not been completed
			{
				spriteBatch.Draw(crosshair, new Rectangle(player.Rect.X, player.Rect.Y - (Globals.TILE_SIZE * 3), 32, 32), Color.White);
			}
			spriteBatch.End();


			spriteBatch.Begin();
			// Draw the hud
			hud.Draw(spriteBatch);
			// Draw the boss health bar if the boss is present
			if (bossHealthBar.Visible)
			{
				bossHealthBar.Draw(spriteBatch);
			}
			spriteBatch.End();
		}

	}
}
