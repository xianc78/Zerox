using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    ///<summary>
    /// Score displayed after an enemy is killed
    ///</summary>
    public class ScoreText {
        string text;
        Rectangle rect;
        double lifeTimer;
        BitmapFont font;
        Level level;
        Game1 game;
        const double LIFE_INTERVAL = 400;
        public ScoreText(int x, int y, Level level, string text) {
            this.text = text;
            lifeTimer = LIFE_INTERVAL;
            rect = new Rectangle(x, y, 10, 10);
            this.level = level;
            game = this.level.Game;
            font = new BitmapFont("Expire", game);
        }

        public void Update(GameTime gameTime) {
            lifeTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
            if (lifeTimer <= 0) {
                level.ScoreTextList.Remove(this);
            }
        }

        public void Draw(SpriteBatch spriteBatch) {
            font.DrawString(text, rect, Color.White, spriteBatch);
        }
    }
}