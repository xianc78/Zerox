﻿using System;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;

namespace Shooter
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		Level level;
		int levelNo;
		string[] levelList;
		KeyboardState keyState;
		KeyboardState prevState;
		GamePadState gamePadState;
		GamePadState prevPadState;
		Mode mode;
		MainMenu menu;
		DebugMenu debugMenu;

		Texture2D pauseTexture;

		HighScoreManager highScoreManager;
		public HighScoreManager HighScoreManager 
		{
			get 
			{
				return highScoreManager;
			}
		}
		BitmapFont highScoreFont;
		ScoreBoard scoreBoard;
		public ScoreBoard ScoreBoard {
			get {
				return scoreBoard;
			}
		}

		public FiringMode FiringMode;

		public int LevelNo
		{
			set 
			{
				if (value >= levelList.Length) 
				{
					levelNo = levelList.Length - 1;
				}
				levelNo = value;
			}
			get
			{
				return levelNo;
			}
		}

		public int MAX_LEVELS {
			get {
				return levelList.Length;
			}
		}

		SoundEffect unPauseSound;
		SoundEffect backSound;
		Song titleMusic;
		Song creditsMusic;

		Credits credits;

		public Mode Mode
		{
			get
			{
				return mode;
			}
			set
			{
				if (value == Mode.Title || value == Mode.HighScore || value == Mode.Controls)
				{
					MediaPlayer.Stop();
				}
				else if (value == Mode.End) {
					MediaPlayer.Play(creditsMusic);
				}
				mode = value;
			}
		}

		public Level Level
		{
			get
			{
				return level;
			}
		}

		Texture2D gameLogo;

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// Set window size
			graphics.PreferredBackBufferWidth = Globals.SCREEN_WIDTH;
			graphics.PreferredBackBufferHeight = Globals.SCREEN_HEIGHT;
			graphics.ApplyChanges();

			// Initialize high score manager
			highScoreManager = new HighScoreManager(this);
			highScoreFont = new BitmapFont("Expire", this);

			// Initialize levels
			levelNo = 0;
			levelList = Directory.GetFiles("Levels/", "Level*.xml");
			Array.Sort(levelList);
			level = new Level(levelList[levelNo], this);

			// Initialize menu
			menu = new MainMenu(this);

			// Initialize debug menu
			debugMenu = new DebugMenu(this, graphics.GraphicsDevice);

			credits = new Credits(this);

			// Initialize scoreboard
			scoreBoard = new ScoreBoard(this);

			// Set mode to title screen
			mode = Mode.Title;

			// Set powere up state to default
			//Globals.PowerUp = PowerUp.Normal;

			// Call the base initialize method
			base.Initialize();

			// Create the pause overlay
			pauseTexture = new Texture2D(graphics.GraphicsDevice, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT);
			Color[] data = new Color[Globals.SCREEN_WIDTH * Globals.SCREEN_HEIGHT];
			for (int i = 0; i < data.Length; i++) {
				data[i] = new Color(0, 0, 0, 127);
			}
			pauseTexture.SetData<Color>(data);

			FiringMode = new NormalFire(this);
			//pauseTexture.SetData<Color>(new Color[] { Color.Transparent });

			// Set the window title
			Window.Title = "Zerox";

			// Play the title screen music
			MediaPlayer.Play(titleMusic);
			MediaPlayer.IsRepeating = true;
		}

		/// <summary>
		/// Reset the game to initial state.
		/// </summary>
		public void Reset()
		{
			MediaPlayer.Stop();
			Globals.Score = 0;
			Globals.Lives = Globals.START_LIVES;
			Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			gameLogo = Content.Load<Texture2D>("Graphics/Zerox_Logo_RGB");

			unPauseSound = Content.Load<SoundEffect>("Sounds/Unpause");
			backSound = Content.Load<SoundEffect>("Sounds/MenuSelect");

			// Load music
			titleMusic = Content.Load<Song>("Music/melodic_power_track_c64_style");
			creditsMusic = Content.Load<Song>("Music/nes_23-chill");

		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			// For Mobile devices, this logic will close the Game when the Back button is pressed
			// Exit() is obsolete on iOS
#if !__IOS__ && !__TVOS__
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();
#endif
			GamePadCapabilities capabilities = GamePad.GetCapabilities(PlayerIndex.One);
			gamePadState = GamePad.GetState(PlayerIndex.One);
			keyState = Keyboard.GetState();
			switch (mode)
			{
				case Mode.Title:
					menu.HandleEvents(keyState, prevState, gamePadState, prevPadState);
					break;
				case Mode.Pause:
					if (keyState.IsKeyDown(Keys.Enter) && !prevState.IsKeyDown(Keys.Enter) || 
					   (gamePadState.IsButtonDown(Buttons.Start) && !prevPadState.IsButtonDown(Buttons.Start)))
					{
						unPauseSound.Play();
						MediaPlayer.Resume();
						mode = Mode.Game;
					}
					break;
				case Mode.Game:
					level.Update(gameTime, keyState, prevState, gamePadState, prevPadState);
					break;
				case Mode.HighScore:
					scoreBoard.HandleEvents(keyState, prevState, gamePadState, prevPadState);
					break;
				case Mode.Controls:
					if ((keyState.IsKeyDown(Keys.Enter) && !prevState.IsKeyDown(Keys.Enter)) || (keyState.IsKeyDown(Keys.X) && !prevState.IsKeyDown(Keys.X))
					|| (keyState.IsKeyDown(Keys.Z) && !prevState.IsKeyDown(Keys.Z))) {
						backSound.Play();
						Reset();
					}
					break;
				case Mode.Debug:
					debugMenu.HandleEvents(keyState, prevState, gamePadState, prevPadState);
					break;
				case Mode.End:
					if ((keyState.IsKeyDown(Keys.X) && !prevState.IsKeyDown(Keys.X)) ||
					   (gamePadState.IsButtonDown(Buttons.A) && !prevPadState.IsButtonDown(Buttons.A))) {
						if (Globals.Score > HighScoreManager.LowestHighScore) {
							ScoreBoard.NameEntry.Enabled = true;
						}
						MediaPlayer.Stop();
						Mode = Mode.HighScore;
					}
					credits.Update(gameTime);
					break;
			}

			prevState = keyState;
			prevPadState = gamePadState;

			base.Update(gameTime);
		}

		/// <summary>
		/// Go to the next level
		/// </summary>
		public void NextLevel()
		{
			levelNo++;
			if (levelNo < levelList.Length) {
				level = new Level(levelList[levelNo], this);
				level.Initialize();
			}
			else {
				Mode = Mode.End;
			}
		}

		public void SetLevel(int levelNo) {
			this.levelNo = levelNo;
			level = new Level(levelList[levelNo], this);
			mode = Mode.Game;
			level.Initialize();
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			switch (mode)
			{
				case Mode.Title:
					graphics.GraphicsDevice.Clear(Color.Black);
					spriteBatch.Begin();
					spriteBatch.Draw(gameLogo, new Vector2(Globals.SCREEN_WIDTH / 2 - 94, 64), Color.White);
					spriteBatch.End();
					menu.Draw(GraphicsDevice, spriteBatch);
					break;
				case Mode.Game:
					level.Draw(spriteBatch, GraphicsDevice);
					break;
				case Mode.HighScore:
					graphics.GraphicsDevice.Clear(Color.Black);
					scoreBoard.Draw(spriteBatch);
					break;
				case Mode.Controls:
					graphics.GraphicsDevice.Clear(Color.Black);
					spriteBatch.Begin();
					highScoreFont.DrawString("Controls", new Rectangle(0, 0, 14, 14), Color.White, spriteBatch);
					highScoreFont.DrawString("Arrow Keys - Move", new Rectangle(14, 16, 14, 14), Color.White, spriteBatch);
					highScoreFont.DrawString("Z - Drop Bomb", new Rectangle(14, 32, 14, 14), Color.White, spriteBatch);
					highScoreFont.DrawString("X - Shoot", new Rectangle(14, 48, 14, 14), Color.White, spriteBatch);
					highScoreFont.DrawString("Enter - Pause/Unpause", new Rectangle(14, 64, 14, 14), Color.White, spriteBatch);
					highScoreFont.DrawString("Press X, Z, or Enter\nto Continue", new Rectangle(0, 134, 14, 14), Color.Yellow, spriteBatch);
					spriteBatch.End();
					break;
				case Mode.Pause:
					level.Draw(spriteBatch, GraphicsDevice);
					spriteBatch.Begin();
					spriteBatch.Draw(pauseTexture, Vector2.Zero, Color.White);
					spriteBatch.End();
					break;
				case Mode.Debug:
					graphics.GraphicsDevice.Clear(Color.Black);
					debugMenu.Draw(GraphicsDevice, spriteBatch);
					break;
				case Mode.End:
					graphics.GraphicsDevice.Clear(Color.DarkBlue);
					spriteBatch.Begin();
					credits.Draw(spriteBatch);
					/*
					highScoreFont.DrawString("Congratulations!", new Rectangle(Globals.SCREEN_WIDTH / 2 - 128, Globals.SCREEN_HEIGHT / 4, 16, 16), Color.White, 
											 spriteBatch);
					highScoreFont.DrawString("Score: " + Globals.Score, new Rectangle(Globals.SCREEN_WIDTH / 2 - 128, Globals.SCREEN_HEIGHT / 4 + 16, 16, 16), 
											 Color.White, spriteBatch);
					highScoreFont.DrawString("Press Fire to Continue", new Rectangle(Globals.SCREEN_WIDTH / 2 - 154, Globals.SCREEN_HEIGHT - 14, 14, 14), Color.Yellow,
											 spriteBatch);
					*/
					spriteBatch.End();
					break;
			}
			base.Draw(gameTime);
		}
	}
}
