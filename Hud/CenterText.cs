﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class CenterText
	{
		string text;
		string displayText;
		SpriteFont font;
		BitmapFont bitFont;

		/// <summary>
		/// Time before the next character is printed
		/// </summary>
		double printTimer;
		const double PRINT_INTERVAL = 25f;
		int printIndex;

		/// <summary>
		/// The life of the text before it disappears
		/// </summary>
		double lifeTimer;
		const double LIFE_INTERVAL = 2000f;

		bool visible;
		/// <summary>
		/// Gets a value indicating whether this <see cref="T:Shooter.CenterText"/> is visible.
		/// </summary>
		/// <value><c>true</c> if visible; otherwise, <c>false</c>.</value>
		public bool Visible
		{
			get
			{
				return visible;
			}
		}

		public CenterText(SpriteFont font, Game1 game)
		{
			this.font = font;
			bitFont = new BitmapFont("Expire" ,game);
		}

		public void Update(GameTime gameTime)
		{
			if (!displayText.Equals(text)) {
				printTimer += gameTime.ElapsedGameTime.TotalMilliseconds;
				if (printTimer >= PRINT_INTERVAL)
				{
					displayText += text[printIndex];
					printTimer = 0f;
					printIndex++;
				}
			}

			lifeTimer += gameTime.ElapsedGameTime.TotalMilliseconds;
			if (lifeTimer >= LIFE_INTERVAL)
			{
				visible = false;
			}
		}

		public void SetText(string text)
		{
			// Set the text to parameter and display text to an empty string
			this.text = text;
			displayText = "";

			// Set print index and life and print timer to 0
			printIndex = 0;
			lifeTimer = 0f;
			printTimer = 0f;

			// Set visible
			visible = true;
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			Vector2 pos = new Vector2((Globals.SCREEN_WIDTH / 2) - (bitFont.MeasureString(text, 14).X / 2), (Globals.SCREEN_HEIGHT / 2) - 
				(bitFont.MeasureString(text, 14).Y / 2));
			bitFont.DrawString(displayText, new Rectangle((int)pos.X, (int)pos.Y, 14, 14), Color.White, spriteBatch);
			//spriteBatch.DrawString(font, displayText, pos, Color.White);
		}
	}
}
