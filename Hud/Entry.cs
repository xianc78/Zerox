namespace Shooter {
    ///<summary>
    /// Represents a highscore entry.
    ///</summary>
    public struct Entry {
        public string name;
        public int score;
    }
}