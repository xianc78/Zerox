﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Hud
	{
		SpriteFont hudFont;
		Game1 game;
		CenterText centerText;
		BitmapFont bitFont;
		Texture2D shipIcon;

		public Hud(Game1 game)
		{
			this.game = game;
			hudFont = this.game.Content.Load<SpriteFont>("Fonts/Font");
			shipIcon = game.Content.Load<Texture2D>("Graphics/HeartIcon");
			bitFont = new BitmapFont("Expire", this.game);
			centerText = new CenterText(hudFont, this.game);
		}

		public void Update(GameTime gameTime)
		{
			if (centerText.Visible)
			{
				centerText.Update(gameTime);
			}

		}

		/// <summary>
		/// Sets and displays a string on the center of the screen
		/// </summary>
		/// <param name="text">Text to be displayed onscreen</param>
		public void SetCenterText(string text)
		{
			centerText.SetText(text);
		}

		/// <summary>
		/// Draw the hud
		/// </summary>
		/// <param name="spriteBatch">Sprite batch used to draw the hud</param>
		public void Draw(SpriteBatch spriteBatch)
		{
			bitFont.DrawString("Scr ", new Rectangle(0, 0, 12, 12), Color.Yellow, spriteBatch);
			bitFont.DrawString(Globals.Score.ToString("0000000"), new Rectangle(48, 0, 12, 12), Color.White, spriteBatch);
			//bitFont.DrawString("Lvl ", new Rectangle(132, 0, 12, 12), Color.Yellow, spriteBatch);
			//bitFont.DrawString((game.LevelNo + 1).ToString("00"), new Rectangle(180, 0, 12, 12), Color.White, spriteBatch);
			bitFont.DrawString("Lvl ", new Rectangle(168, 0, 12, 12), Color.Yellow, spriteBatch);
			bitFont.DrawString((game.LevelNo + 1).ToString("00"), new Rectangle(216, 0, 12, 12), Color.White, spriteBatch);
			//spriteBatch.Draw(shipIcon, new Rectangle(216, 0, 12, 12), Color.White);
			//bitFont.DrawString("x" + Globals.Lives.ToString("00"), new Rectangle(228, 0, 12, 12), Color.White, spriteBatch);
			spriteBatch.Draw(shipIcon, new Rectangle(272, 0, 12, 12), Color.White);
			bitFont.DrawString("x" + Globals.Lives.ToString("00"), new Rectangle(284, 0, 12, 12), Color.White, spriteBatch);

			// Draw center text (if any)
			if (centerText.Visible)
			{
				centerText.Draw(spriteBatch);
			}
		}
	}
}
