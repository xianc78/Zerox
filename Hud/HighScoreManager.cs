using System;
using System.Collections.Generic;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    ///<summary>
    /// Manages highscores.
    ///</summary>
    public class HighScoreManager {
        Game1 game;
        Entry[] highScores;
        XmlDocument xmlDoc;

        public int LowestHighScore {
            get {
                return highScores[9].score;
            }
        }

        public HighScoreManager (Game1 game) {
            this.game = game;
            highScores = new Entry[10];
            xmlDoc = new XmlDocument();
            xmlDoc.Load("Scores.xml");
            LoadHighScore();
        }
        ///<summary>
        /// Loads highscores from the Scores.xml file
        ///</summary>
        public void LoadHighScore() {
            XmlNodeList entries = xmlDoc.DocumentElement.GetElementsByTagName("entry");
            for (int i = 0; i < 10; i++) {
                if (i > entries.Count - 1) {
                    break;
                }
                highScores[i].name = entries[i].Attributes.GetNamedItem("name").Value.ToUpper();
                highScores[i].score = Convert.ToInt32(entries[i].Attributes.GetNamedItem("score").Value);
            }
            sort();
        }

        void sort() {
            for (int i = 0; i < highScores.Length - 1; i++) 
            {
                for (int j = i + 1; j > 0; j--) 
                {
                    if (highScores[j - 1].score < highScores[j].score) 
                    {
                        Entry temp = highScores[j - 1];
                        highScores[j - 1] = highScores[j];
                        highScores[j] = temp;
                    }
                }
            }
        }

        public void SaveHighScore() {
            XmlTextWriter xmlText = new XmlTextWriter("Scores.xml", null);
            xmlDoc.DocumentElement.RemoveAll();
            for (int i = 0; i < 10; i++) {
                XmlElement elem = xmlDoc.CreateElement("entry");
                /*
                elem.Attributes.Append(xmlDoc.CreateAttribute("name", highScores[i].name));
                elem.Attributes.Append(xmlDoc.CreateAttribute("score", Convert.ToString(highScores[i].score)));
                */
                elem.SetAttribute("name", highScores[i].name);
                elem.SetAttribute("score", Convert.ToString(highScores[i].score));
                xmlDoc.DocumentElement.AppendChild(elem);
            }
            xmlDoc.Save(xmlText);
        }

        public void AddHighScore(string name, int score) {
            List<Entry> newHighScoreList = new List<Entry>(highScores);
            Entry newEntry;
            newEntry.name = name;
            newEntry.score = score;
            newHighScoreList.Add(newEntry);
            for (int i = 0; i < newHighScoreList.Count - 1; i++) 
            {
                for (int j = i + 1; j > 0; j--) 
                {
                    if (newHighScoreList[j - 1].score < newHighScoreList[j].score) 
                    {
                        Entry temp = newHighScoreList[j - 1];
                        newHighScoreList[j - 1] = newHighScoreList[j];
                        newHighScoreList[j] = temp;
                    }
                }
            }
            newHighScoreList.RemoveAt(newHighScoreList.Count - 1);
            highScores = newHighScoreList.ToArray();
            SaveHighScore();
        }

        public string GetHighScores() {
            String returnString = "Rank" + '\t' + "Name" + '\t' + "Score" + '\n';
            for (int i = 0; i < 10; i++) {
                returnString += (i + 1).ToString("00") + "    " + highScores[i].name + "    " + highScores[i].score + '\n';
            }
            return returnString;
        }
    }
}