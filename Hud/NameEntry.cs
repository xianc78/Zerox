using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Shooter {
    ///<summary>
    /// Handles the name entry if the player gets a highscore
    ///</sumamary>
    public class NameEntry {
        int cursor;
        int[] initials = {0, 0, 0};
        Game1 game;
        bool enabled;
        string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public bool Enabled 
        {
            set 
            {
                enabled = value;
            }
            get 
            {
                return enabled;
            }
        }
        BitmapFont font;
        SoundEffect exitSound;

        public NameEntry(Game1 game, BitmapFont font) 
        {
            this.game = game;
            this.cursor = 0;
            this.font = font;
            exitSound = game.Content.Load<SoundEffect>("Sounds/MenuSelect");
            enabled = false;
        }

        public void HandleEvents(KeyboardState keyState, KeyboardState prevState, GamePadState gamePadState, GamePadState prevPadState) 
        {
            if ((keyState.IsKeyDown(Keys.Up) && !prevState.IsKeyDown(Keys.Up)) || 
                (gamePadState.IsButtonDown(Buttons.DPadUp) && !prevPadState.IsButtonDown(Buttons.DPadUp))) {
                initials[cursor]++;
                if (initials[cursor] > alphabet.Length) {
                    initials[cursor] = 0;
                }
            }
            else if ((keyState.IsKeyDown(Keys.Down) && !prevState.IsKeyDown(Keys.Down)) || 
                    (gamePadState.IsButtonDown(Buttons.DPadDown) && !prevPadState.IsButtonDown(Buttons.DPadDown))) {
                initials[cursor]--;
                if (initials[cursor] < 0) {
                    initials[cursor] = alphabet.Length - 1;
                }
            }
            else if ((keyState.IsKeyDown(Keys.Left) && !prevState.IsKeyDown(Keys.Left)) || 
                    (gamePadState.IsButtonDown(Buttons.DPadLeft) && !prevPadState.IsButtonDown(Buttons.DPadLeft))) {
                cursor--;
                if (cursor < 0) {
                    cursor = 0;
                }
            }
            else if ((keyState.IsKeyDown(Keys.Right) && !prevState.IsKeyDown(Keys.Right)) || 
                    (gamePadState.IsButtonDown(Buttons.DPadRight) && !prevPadState.IsButtonDown(Buttons.DPadRight))) {
                cursor++;
                if (cursor > 2) {
                    cursor = 2;
                }
            }
            if ((keyState.IsKeyDown(Keys.X) && !prevState.IsKeyDown(Keys.X))  || 
                (gamePadState.IsButtonDown(Buttons.A) && !prevPadState.IsButtonDown(Buttons.A))) {
                game.HighScoreManager.AddHighScore(alphabet[initials[0]].ToString() + alphabet[initials[1]].ToString() + 
                                                   alphabet[initials[2]].ToString(), Globals.Score);
                enabled = false;
                exitSound.Play();
                game.Reset();
            }
        }

        public void Draw(SpriteBatch spriteBatch) 
        {
            string x = "Enter your name: ";
            font.DrawString("v", new Rectangle((int)font.MeasureString(x, 16).X + (cursor * 16), 304, 16, 16), Color.White, spriteBatch);
            font.DrawString(x, new Rectangle(0, 320, 16, 16), Color.White, spriteBatch);
            font.DrawString(alphabet[initials[0]].ToString() + alphabet[initials[1]].ToString() + alphabet[initials[2]].ToString(), 
                            new Rectangle((int)font.MeasureString(x, 16).X, 320, 16, 16), Color.White, spriteBatch);
        }
    }
}