﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class BossHealthBar
	{
		bool visible;
		public bool Visible
		{
			get
			{
				return visible;
			}
			set
			{
				visible = value;
			}
		}

		Level level;
		Game1 game;
		Texture2D texture;
		Boss boss;

		public BossHealthBar(Level level)
		{
			this.level = level;
			game = this.level.Game;
			boss = level.Boss;
			texture = game.Content.Load<Texture2D>("Graphics/BossHealthBar");
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(texture, new Rectangle(0, Globals.SCREEN_HEIGHT - 10, Globals.SCREEN_WIDTH, 10), new Rectangle(0, 0, Globals.SCREEN_WIDTH, 10), Color.White);
			spriteBatch.Draw(texture, new Rectangle(1, Globals.SCREEN_HEIGHT - 9, (int)(((float)boss.Hp / boss.MAX_HP) * (Globals.SCREEN_WIDTH - 2)), 8), 
			                 new Rectangle(1, 10, Globals.SCREEN_WIDTH - 2, 10), Color.White);
		}
	}
}