using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Shooter 
{
    ///<summary>
    /// Leaderboard that is displayed on the highscore game mode.
    ///</summary>
    public class ScoreBoard {
        BitmapFont font;
        Game1 game;
        NameEntry nameEntry;
        public NameEntry NameEntry 
        {
            get 
            {
                return nameEntry;
            }
        }
        SoundEffect exitSound;
        public ScoreBoard(Game1 game) {
            this.game = game;
            font = new BitmapFont("Expire", this.game);
            nameEntry = new NameEntry(this.game, font);
            exitSound = game.Content.Load<SoundEffect>("Sounds/MenuSelect");
        }

        public void HandleEvents(KeyboardState keyState, KeyboardState prevState, GamePadState gamePadState, GamePadState prevPadState) 
        {
            if (nameEntry.Enabled) 
            {
                nameEntry.HandleEvents(keyState, prevState, gamePadState, prevPadState);
            }
            else {
                if ((keyState.IsKeyDown(Keys.X) && !prevState.IsKeyDown(Keys.X)) || (gamePadState.IsButtonDown(Buttons.A) && !prevPadState.IsButtonDown(Buttons.A))) {
                    exitSound.Play();
                    game.Reset();
                }
            }

        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Begin();
            font.DrawString(game.HighScoreManager.GetHighScores(), new Rectangle(0, Globals.SCREEN_HEIGHT / 4, 16, 16), Color.White, spriteBatch);
            if (nameEntry.Enabled) {
                nameEntry.Draw(spriteBatch);
            }
            spriteBatch.End();
        }
    }
}