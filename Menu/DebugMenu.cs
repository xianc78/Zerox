﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class DebugMenu : Menu {
        int levelCursor;
        public int LevelCursor {
            get {
                return levelCursor;
            }
            set {
                levelCursor = value;
            }
        }
        int powerUpCursor;
        public int PowerUpCursor {
            get {
                return powerUpCursor;
            }
            set {
                powerUpCursor = value;
            }
        }
        Texture2D texture;

        public DebugMenu(Game1 game, GraphicsDevice graphicsDevice) : base(game) {
            cursor = 0;
            levelCursor = 0;
            powerUpCursor = 0;
            font = new BitmapFont("ArtosSans", this.game);
            menuItems = new MenuItem[] {new LevelSelect(new Vector2(16, 2), this.game, this), new PowerUpSelect(new Vector2(16, 16), this.game, this), 
            new ViewCredits(new Vector2(16, 30), this.game), new ExitDebug(new Vector2(16, 44), this.game, this)};

            texture = new Texture2D(graphicsDevice, 256, 60);
            Color[] data = new Color[256 * 60];
            /*
            for (int i = 0; i < data.Length; i++) {
                if (i < 252 || (i >= 13856 && i < 14112) || (i % 252 == 0) || (i % 251 == 0)) {
                    data[i] = Color.White;
                }
                else {
                    data[i] = Color.Blue;
                }
            }
            texture.SetData<Color>(data);
            */
            for (int y = 0; y < texture.Height; y++) {
                for (int x = 0; x < texture.Width; x++) {
                    if (x == 0 || y == 0 || y == texture.Height - 1 || x == texture.Width - 1) {
                        data[y * texture.Width + x] = Color.White;
                    }
                    else {
                        data[y * texture.Width + x] = Color.Teal;
                    }
                }
            }
            texture.SetData<Color>(data);

            viewPort = new Viewport(0, 28, 256, 60);
        }

        public override void HandleEvents(KeyboardState keyState, KeyboardState prevState, GamePadState gamePadState, GamePadState prevPadState) {
            
            base.HandleEvents(keyState, prevState, gamePadState, prevPadState);
            
            /*
            if (keyState.IsKeyDown(Keys.Up) && !prevState.IsKeyDown(Keys.Up)) {
                cursor--;
                if (cursor < 0) {
                    cursor = 0;
                }
            }
            else if (keyState.IsKeyDown(Keys.Down) && !prevState.IsKeyDown(Keys.Down)) {
                cursor++;
                if (cursor > 2) {
                    cursor = 2;
                }
            }
            else if (keyState.IsKeyDown(Keys.Left) && !prevState.IsKeyDown(Keys.Left)) {
                switch (cursor) {
                    case 0:
                        levelCursor--;
                        if (levelCursor < 0) {
                            levelCursor = 0;
                        }
                        break;
                    case 1:
                        powerUpCursor--;
                        if (powerUpCursor < 0) {
                            powerUpCursor = 0;
                        }
                        break;
                }
            }
            else if (keyState.IsKeyDown(Keys.Right) && !prevState.IsKeyDown(Keys.Right)) {
                switch (cursor) {
                    case 0:
                        levelCursor++;
                        if (levelCursor >= game.MAX_LEVELS) {
                            levelCursor = game.MAX_LEVELS - 1;
                        }
                        break;
                    case 1:
                        powerUpCursor++;
                        if (powerUpCursor > 4) {
                            powerUpCursor = 4;
                        }
                        break;
                }
                
            }
            if (keyState.IsKeyDown(Keys.X) && !prevState.IsKeyDown(Keys.X)) {
                if (cursor == 2) {
                    game.Mode = Mode.End;
                }
                else {
                    setPowerUp();
                    game.SetLevel(levelCursor);
                }
            }
            */

        }

        void setPowerUp() {
            switch ((PowerUp)powerUpCursor) {
                case PowerUp.Normal:
                    game.FiringMode = new NormalFire(game);
                    break;
                case PowerUp.TwinBlaster:
                    game.FiringMode = new TwinFire(game);
                    break;
                case PowerUp.MachineGun:
                    game.FiringMode = new MachineGunFire(game);
                    break;
                case PowerUp.RearBlaster:
                    game.FiringMode = new RearFire(game);
                    break;
                case PowerUp.SideBlaster:
                    game.FiringMode = new SprayFire(game);
                    break;

            }
        }

        public void Exit() {
            setPowerUp();
            game.SetLevel(levelCursor);
        }

        public override void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch) {

            spriteBatch.Begin();
            font.DrawString("Get out of my\ndebug menu, cheater!", new Rectangle(0, 0, 14, 14), Color.Red, spriteBatch);
            spriteBatch.End();
            spriteBatch.Begin();
            Viewport defaultViewport = graphicsDevice.Viewport; // Create temporary variable for the default viewport
            graphicsDevice.Viewport = viewPort;
            spriteBatch.Draw(texture, new Vector2(0, 0), Color.White);
            
			// Draw the menu items
			for (int i = 0; i < menuItems.Length; i++)
			{
				menuItems[i].Draw(spriteBatch);
			}
			font.DrawString(">", new Rectangle(2, cursor * 14 + 2, 14, 14), Color.White, spriteBatch);
			spriteBatch.End();
            graphicsDevice.Viewport = defaultViewport;

            /*
            Viewport defaultViewport = graphicsDevice.Viewport; // Create temporary variable for the default viewport
            spriteBatch.Begin();
            font.DrawString("Get the fuck out of\nmy debug menu, cheater!", new Rectangle(0, 0, 14, 14), Color.Red, spriteBatch);
            font.DrawString(">", new Rectangle(0, (cursor * 14) + 28, 14, 14), Color.White, spriteBatch);
            font.DrawString("Level: " + (levelCursor + 1), new Rectangle(14, 28, 14, 14), Color.White, spriteBatch);
            font.DrawString("Power Up: " + (PowerUp)powerUpCursor, new Rectangle(14, 42, 14, 14), Color.White, spriteBatch);
            font.DrawString("Credits", new Rectangle(14, 56, 14, 14), Color.White, spriteBatch);
            spriteBatch.End();
            */
        }
    }
}