using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class Menu {
        protected Game1 game;
        protected BitmapFont font;
        protected int cursor;

        protected MenuItem[] menuItems;
		protected SoundEffect menuMove;

        protected Viewport viewPort;

        public Menu (Game1 game) {
            this.game = game;
            menuMove = game.Content.Load<SoundEffect>("Sounds/MenuMove");
        }


        public virtual void HandleEvents(KeyboardState keyState, KeyboardState prevState, GamePadState gamePadState, GamePadState prevPadState) {
            if (keyState.IsKeyDown(Keys.Up) && !prevState.IsKeyDown(Keys.Up) || (gamePadState.IsButtonDown(Buttons.DPadUp) && !prevPadState.IsButtonDown(Buttons.DPadUp)))
			{
				menuMove.Play();
				cursor--;
				if (cursor < 0)
				{
					cursor = menuItems.Length - 1;
				}
			}
			else if (keyState.IsKeyDown(Keys.Down) && !prevState.IsKeyDown(Keys.Down) || (gamePadState.IsButtonDown(Buttons.DPadDown) && !prevPadState.IsButtonDown(Buttons.DPadDown)))
			{
				menuMove.Play();
				cursor++;
				if (cursor > menuItems.Length - 1)
				{
					cursor = 0;
				}
			}

            menuItems[cursor].HandleEvents(keyState, prevState);

			if ((keyState.IsKeyDown(Keys.X) && !prevState.IsKeyDown(Keys.X)) || (keyState.IsKeyDown(Keys.Enter) && !prevState.IsKeyDown(Keys.Enter))
				|| (gamePadState.IsButtonDown(Buttons.A) && !prevPadState.IsButtonDown(Buttons.A)))
			{
				menuItems[cursor].Select();
			}
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch) {

        }

       }
}