﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class MenuItem
	{
		protected string text;
		protected string displayText;
		protected Game1 game;
		protected Vector2 position;
		protected BitmapFont font;
		SoundEffect selectSound;

		public MenuItem(Vector2 position, Game1 game)
		{
			this.game = game;
			font = new BitmapFont("Expire", this.game);
			this.position = position;
			selectSound = game.Content.Load<SoundEffect>("Sounds/MenuSelect");
		}

		public virtual void HandleEvents(KeyboardState keyState, KeyboardState prevState) {

		}

		/// <summary>
		/// Virtual method called when item is selected
		/// </summary>
		public virtual void Select()
		{
			selectSound.Play();
		}

		public virtual void Scroll(int offset)
		{

		}

		public virtual void Draw(SpriteBatch spriteBatch)
		{
			//spriteBatch.DrawString(font, text, position, Color.White);
			font.DrawString(text, new Rectangle((int)position.X, (int)position.Y, 16, 16), Color.White, spriteBatch);
		}
	}

	public class StartGame : MenuItem
	{
		public StartGame(Vector2 position, Game1 game) : base(position, game)
		{
			text = "Start";
		}

		public override void Select()
		{
			base.Select();
			game.Mode = Mode.Game;
			game.Level.Initialize();
		}
	}

	public class HighScore : MenuItem
	{
		public HighScore(Vector2 position, Game1 game) : base(position, game)
		{
			text = "High Score";
		}

		public override void Select() 
		{
			base.Select();
			game.Mode = Mode.HighScore;
		}
	}

	public class ViewControls : MenuItem {
		public ViewControls(Vector2 position, Game1 game) : base(position, game) 
		{
			text = "How to Play";
		}

		public override void Select() {
			base.Select();
			game.Mode = Mode.Controls;
		}
	}

	public class Exit : MenuItem
	{
		public Exit(Vector2 position, Game1 game) : base(position, game)
		{
			text = "Exit";
		}

		public override void Select()
		{
			base.Select();
			game.Exit();
		}
	}

	public class LevelSelect : MenuItem {
		int cursor;
		public int Cursor {
			get {
				return cursor;
			}
		}
		DebugMenu debugMenu;
		public LevelSelect(Vector2 position, Game1 game, DebugMenu debugMenu) : base(position, game) {
			cursor = 0;
			font = new BitmapFont("ArtosSans", this.game);
			text = "Level: ";
			this.debugMenu = debugMenu;
		}

		public override void HandleEvents(KeyboardState keyState, KeyboardState prevState) {
			if (keyState.IsKeyDown(Keys.Left) && !prevState.IsKeyDown(Keys.Left)) {
				debugMenu.LevelCursor--;
				if (debugMenu.LevelCursor < 0) {
					debugMenu.LevelCursor = 0;
				}
			}
			else if (keyState.IsKeyDown(Keys.Right) && !prevState.IsKeyDown(Keys.Right)) {
				debugMenu.LevelCursor++;
				if (debugMenu.LevelCursor >= game.MAX_LEVELS) {
					debugMenu.LevelCursor = game.MAX_LEVELS - 1;
				}
			}
			text = "Level: " + (debugMenu.LevelCursor + 1);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			//spriteBatch.DrawString(font, text, position, Color.White);
			font.DrawString(text, new Rectangle((int)position.X, (int)position.Y, 14, 14), Color.White, spriteBatch);
		}
	}

	public class PowerUpSelect : MenuItem {
		DebugMenu debugMenu;
		public PowerUpSelect(Vector2 position, Game1 game, DebugMenu debugMenu) : base(position, game) {
			font = new BitmapFont("ArtosSans", this.game);
			this.debugMenu = debugMenu;
			text = "PwUp: " + (PowerUp)debugMenu.PowerUpCursor;
			
		}

		public override void HandleEvents(KeyboardState keyState, KeyboardState prevState) {
			if (keyState.IsKeyDown(Keys.Left) && !prevState.IsKeyDown(Keys.Left)) {
				debugMenu.PowerUpCursor--;
				if (debugMenu.PowerUpCursor < 0) {
					debugMenu.PowerUpCursor = 0;
				}
			}
			else if (keyState.IsKeyDown(Keys.Right) && !prevState.IsKeyDown(Keys.Right)) {
				debugMenu.PowerUpCursor++;
				if (debugMenu.PowerUpCursor > 4) {
					debugMenu.PowerUpCursor = 4;
				}
			}
			text = "PwUp: " + (PowerUp)debugMenu.PowerUpCursor;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			//spriteBatch.DrawString(font, text, position, Color.White);
			font.DrawString(text, new Rectangle((int)position.X, (int)position.Y, 14, 14), Color.White, spriteBatch);
		}
	}

	public class ViewCredits : MenuItem {
		public ViewCredits(Vector2 position, Game1 game) : base(position, game) {
			text = "Credits";
			font = new BitmapFont("ArtosSans", this.game);
		}

		public override void Select() {
			game.Mode = Mode.End;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			//spriteBatch.DrawString(font, text, position, Color.White);
			font.DrawString(text, new Rectangle((int)position.X, (int)position.Y, 14, 14), Color.White, spriteBatch);
		}
	}

	public class ExitDebug : MenuItem {
		DebugMenu debugMenu;
		public ExitDebug(Vector2 position, Game1 game, DebugMenu debugMenu) : base(position, game) {
			text = "Exit";
			font = new BitmapFont("ArtosSans", this.game);
			this.debugMenu = debugMenu;
		}

		public override void Select() {
			debugMenu.Exit();
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			//spriteBatch.DrawString(font, text, position, Color.White);
			font.DrawString(text, new Rectangle((int)position.X, (int)position.Y, 14, 14), Color.White, spriteBatch);
		}
	}
}
