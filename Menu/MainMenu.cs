﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace Shooter
{
	public class MainMenu : Menu
	{
		
		public MainMenu(Game1 game) : base(game)
		{
			this.game = game;
			font = new BitmapFont("Expire", this.game);
			cursor = 0;
			menuItems = new MenuItem[] { new StartGame(new Vector2(16, 0), this.game), new HighScore(new Vector2(16, 16), this.game), new ViewControls(new Vector2(16, 32), this.game),
			 new Exit(new Vector2(16, 48), this.game) };
			viewPort = new Viewport(new Rectangle((Globals.SCREEN_WIDTH / 2) - 90, Globals.SCREEN_HEIGHT - 80, 208, 64));
			
		}

		public override void HandleEvents(KeyboardState keyState, KeyboardState prevState, GamePadState gamePadState, GamePadState prevPadState)
		{
			base.HandleEvents(keyState, prevState, gamePadState, prevPadState);
			/*
			if (keyState.IsKeyDown(Keys.D) && !prevState.IsKeyDown(Keys.D)) {
				MediaPlayer.Stop();
				game.Mode = Mode.Debug;
			}
			*/
		}

		public override void Draw(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch)
		{
			Viewport defaultViewport = graphicsDevice.Viewport; // Create temporary variable for the default viewport
			graphicsDevice.Viewport = viewPort; // Set viewport for the menu

			spriteBatch.Begin();
			// Draw the menu items
			for (int i = 0; i < menuItems.Length; i++)
			{
				menuItems[i].Draw(spriteBatch);
			}
			font.DrawString(">", new Rectangle(0, cursor * 16, 16, 16), Color.White, spriteBatch);
			//spriteBatch.DrawString(font, ">", new Vector2(0, cursor * 16), Color.White); // Draw the cursor
			spriteBatch.End();

			graphicsDevice.Viewport = defaultViewport; // Set viewport back to the default
		}
	}
}
