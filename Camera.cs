﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Camera
	{
		Rectangle rect;
		public Rectangle Rect
		{
			get
			{
				return rect;
			}
		}
		Level level;

		const float SPEED = -4f;


		/// <summary>
		/// Transformation matrix that is to be applied to sprite batch calls
		/// </summary>
		/// <value>The transform matrix.</value>
		public Matrix TransformMatrix
		{
			get
			{
				return Matrix.CreateTranslation(-rect.X, -rect.Y, 0);
			}
		}

		Vector2 velocity;
		public Vector2 Velocity
		{
			get
			{
				return velocity;
			}
		}

		public Camera(int x, int y, Level level)
		{
			this.level = level;
			rect = new Rectangle(x, y, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT);
			velocity = new Vector2(0f, SPEED);
		}

		/// <summary>
		/// Sets the camera's velocity to zero.
		/// </summary>
		public void Stop()
		{
			velocity = Vector2.Zero;
		}

		/// <summary>
		/// Sets the camera's velocity to the default value.
		/// </summary>
		public void Resume()
		{
			velocity = new Vector2(0f, SPEED);
		}

		/// <summary>
		/// Moves the camera's rectangular coordinates based on it's velocity
		/// </summary>
		public void Scroll()
		{
			rect.X += (int)velocity.X;
			rect.Y += (int)velocity.Y;
		}

		public void SetPosition(int x, int y) {
			rect.X = x;
			rect.Y = y;
		}
	}
}