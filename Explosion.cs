﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Shooter
{
	public class Explosion : GameObject
	{
		SoundEffect explosionSound;
		Rectangle[] frames = { new Rectangle(0, 0, Globals.TILE_SIZE, Globals.TILE_SIZE), new Rectangle(34, 0, Globals.TILE_SIZE, Globals.TILE_SIZE), 
			new Rectangle(68, 0, Globals.TILE_SIZE, Globals.TILE_SIZE), new Rectangle(102, 0, Globals.TILE_SIZE, Globals.TILE_SIZE), new Rectangle(136, 0, Globals.TILE_SIZE, Globals.TILE_SIZE), 
			new Rectangle(170, 0, Globals.TILE_SIZE, Globals.TILE_SIZE), new Rectangle(204, 0, Globals.TILE_SIZE, Globals.TILE_SIZE)};
		double animationTimer;
		const double ANIMATION_INTERVAL = 100;
		int index;

		public Explosion(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/Explosion");
			rect = new Rectangle(x, y, Globals.TILE_SIZE, Globals.TILE_SIZE);
			animationTimer = 0;
			index = 0;
			explosionSound = game.Content.Load<SoundEffect>("Sounds/Explosion");
			explosionSound.Play();
		}

		public override void Update(GameTime gameTime)
		{
			animationTimer += gameTime.ElapsedGameTime.TotalMilliseconds;
			if (animationTimer >= ANIMATION_INTERVAL)
			{
				index++;
				if (index >= frames.Length)
				{
					level.ExplosionList.Remove(this);
				}
			}
		}

		public static void SpawnExplosion(int x, int y, Level level)
		{
			level.ExplosionList.Add(new Explosion(x, y, level));
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(texture, rect, frames[index], Color.White);
		}
	}
}