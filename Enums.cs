﻿namespace Shooter
{
	public enum PowerUp { Normal, TwinBlaster, MachineGun, RearBlaster, SideBlaster, SuperBlaster };

	public enum DropItem { None, OneUp, TwinBlaster, MachineGun, SideBlaster, RearBlaster };

	public enum LevelState { Normal, Completed, Dead };

	public enum BulletShip { Player, Enemy };

	public enum Mode { Title, Game, Pause, Controls, GameOver, Debug, HighScore, End };

	public enum Direction { Up, Down, Left, Right };
}