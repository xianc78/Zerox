using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace Shooter {
    public class Credits {
        BitmapFont font;
        Game1 game;
        int y;
        double creditsTimer;
        double creditsInterval = 5000f;
        List<CreditsText> creditsTextList;

        public Credits(Game1 game) {
            this.game = game;
            y = 0;
            font = new BitmapFont("Expire", this.game);

            creditsTextList = new List<CreditsText>();
            int lineY = Globals.SCREEN_HEIGHT;
            // Create credits text
            foreach (string line in File.ReadLines("Credits.txt")) {
                if (line.StartsWith('#')) {
                    creditsTextList.Add(new CreditsText(Globals.SCREEN_WIDTH - (int)font.MeasureString(line.Substring(1), 16).X, lineY, line.Substring(1), font, Color.Yellow));
                }
                else {
                    if (line.EndsWith("+")) {
                        lineY += 16;
                        creditsTextList.Add(new CreditsText(0, lineY, line.Substring(0, line.Length - 1), font, Color.White));
                    }
                    else {
                        creditsTextList.Add(new CreditsText(0, lineY, line, font, Color.White));
                        if (line.Equals("------------")) {
                            lineY += 16;
                        }
                    }
                }
                lineY += 16;
            }
            creditsTextList.Add(new CreditsText(Globals.SCREEN_WIDTH / 2 - 144, lineY + Globals.SCREEN_WIDTH / 2, "Thanks For Playing", font, Color.White));

            creditsTimer = creditsInterval;
        }

        public void Update(GameTime gameTime) {
            creditsTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
            if (creditsTimer <= 0f) {
                y -= 1;
                foreach (CreditsText text in creditsTextList) {
                    text.Move(-1);
                }
                if (creditsTextList[creditsTextList.Count - 1].Rect.Bottom < -1) {
                    if (Globals.Score > game.HighScoreManager.LowestHighScore) {
						game.ScoreBoard.NameEntry.Enabled = true;
					}
					MediaPlayer.Stop();
					game.Mode = Mode.HighScore;
                    //game.Reset();
                }
            }
            
        }

        public void Draw(SpriteBatch spriteBatch) {
            font.DrawString("Congratulations!", new Rectangle(Globals.SCREEN_WIDTH / 2 - 128, Globals.SCREEN_HEIGHT / 4 + y, 16, 16), Color.White, 
									 spriteBatch);
			font.DrawString("Score: " + Globals.Score, new Rectangle(Globals.SCREEN_WIDTH / 2 - 128, Globals.SCREEN_HEIGHT / 4 + 16 + y, 16, 16), 
									 Color.White, spriteBatch);

            foreach (CreditsText text in creditsTextList) {
                text.Draw(spriteBatch);
            }

            /*
            font.DrawString("Credits", new Rectangle(0, Globals.SCREEN_HEIGHT + y, 16, 16), 
									 Color.White, spriteBatch);
            font.DrawString("Design, levels, & \nprogramming", new Rectangle(0, Globals.SCREEN_HEIGHT + 34 + y, 16, 16), Color.White, spriteBatch);
            font.DrawString("xianc78", new Rectangle(Globals.SCREEN_WIDTH - 112, Globals.SCREEN_HEIGHT + 66 + y, 16, 16), Color.Yellow, spriteBatch);
            font.DrawString("Art assets", new Rectangle(0, Globals.SCREEN_HEIGHT + 84 + y, 16, 16), Color.White, spriteBatch);
            //font.DrawString("Player & Enemy Ships", new Rectangle(0, Globals.SCREEN_HEIGHT + 102 + y, 16, 16), Color.White, spriteBatch);
            //font.DrawString("Master484", new Rectangle(Globals.SCREEN_WIDTH - 144, Globals.SCREEN_HEIGHT + 118 + y, 16, 16), Color.Yellow, spriteBatch);
            font.DrawString("Master484", new Rectangle(Globals.SCREEN_WIDTH - 144, Globals.SCREEN_HEIGHT + 102 + y, 16, 16), Color.Yellow, spriteBatch);
            */

        }
    }

    public class CreditsText {
        public Rectangle Rect;
        string text;
        BitmapFont font;
        Color color;
        public CreditsText (int x, int y, string text, BitmapFont font, Color color) {
            this.text = text;
            this.font = font;
            this.Rect = new Rectangle(x, y, 16, 16);
            this.color = color;
        }

        public void Move(int offset) {
            Rect.Y += offset;
        }

        public void Draw(SpriteBatch spriteBatch) {
            font.DrawString(text, Rect, color, spriteBatch);
        }
    }
}