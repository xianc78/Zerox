﻿using System;

namespace Shooter
{
	public static class Globals
	{
		public const int SCREEN_WIDTH = 320;
		public const int SCREEN_HEIGHT = 480;

		public const float BULLET_SPEED = 12f;

		/// <summary>
		/// The width and height for the tiles.
		/// </summary>
		public const int TILE_SIZE = 32;

		public const int START_LIVES = 5;

		public static int Score = 0;
		public static int Lives = START_LIVES;

		/// <summary>
		/// The current power up of the player.
		/// </summary>
		//public static PowerUp PowerUp = PowerUp.Normal;
	}
}