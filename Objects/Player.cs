﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace Shooter
{
	public class Player : GameObject
	{
		SoundEffect ShootSound;
		public SoundEffectInstance MachineGun;
		SoundEffect DamageSound;
		SoundEffect BombSound;
		SoundEffect criticalHit;


		double machineGunTimer;
		public const double MACHINEGUNINTERVAL = 50;
		public double MachineGunTimer {
			get {
				return machineGunTimer;
			}
		}

		double recoveryTimer;
		const double RECOVERYINTERVAL = 1000f;

		double blinkTimer;
		const double BLINKINTERVAL = 100f;
		bool invisible;

		double animationTimer;
		const double ANIMATION_INTERVAL = 200;
		int animationIndex;
		//Rectangle[] animationFrames = {new Rectangle(21, 37, 8, 8), new Rectangle(21, 25, 8, 8), new Rectangle(21, 13, 8, 8), new Rectangle(21, 1, 8, 8)};
		Rectangle[] animationFrames = {new Rectangle(33, 37, 8, 8), new Rectangle(33, 25, 8, 8), new Rectangle(33, 13, 8, 8), new Rectangle(33, 1, 8, 8)};
		Rectangle frame;

		int MAX_FRAMES 
		{
			get 
			{
				return animationFrames.Length;
			}
		}


		SpriteFont powerUpFont;
		BitmapFont bitFont;
		string powerUpText;
		double powerUpTextTimer;
		const double powerUpTextInterval = 750f;

		public Point TrailStart;
		public bool HasTrail;

		Texture2D trail;

		bool isDead;
		public bool IsDead
		{
			set {
				isDead = value;
			}
			get
			{
				return isDead;
			}
		}
		FiringMode firingMode;
		public FiringMode FiringMode {
			get 
			{
				return firingMode;
			}
			set 
			{
				firingMode = value;
			}
		}

		Texture2D spark;
		Rectangle sparkRect;

		/// <summary>
		/// Gets a value indicating whether this <see cref="T:Shooter.Player"/> is recovering.
		/// </summary>
		/// <value><c>true</c> if is recovering; otherwise, <c>false</c>.</value>
		public bool IsRecovering
		{
			get
			{
				return recoveryTimer > 0;
			}
		}

		public Player(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/PlayerShip");
			powerUpFont = game.Content.Load<SpriteFont>("Fonts/PowerUp");
			rect = new Rectangle(x, y, Globals.TILE_SIZE, Globals.TILE_SIZE);

			// Load sound effects
			ShootSound = game.Content.Load<SoundEffect>("Sounds/Bullet");
			MachineGun = game.Content.Load<SoundEffect>("Sounds/MachineGun").CreateInstance();
			DamageSound = game.Content.Load<SoundEffect>("Sounds/PlayerHit");
			BombSound = game.Content.Load<SoundEffect>("Sounds/Bomb");
			criticalHit = game.Content.Load<SoundEffect>("Sounds/CriticalHit");

			trail = game.Content.Load<Texture2D>("Graphics/Level1Bullet");

			recoveryTimer = 0;
			isDead = false;
			invisible = false;
			blinkTimer = BLINKINTERVAL;

			animationIndex = 0;
			frame = animationFrames[animationIndex];

			bitFont = new BitmapFont("Expire", this.game);

			firingMode = level.Game.FiringMode;

			spark = game.Content.Load<Texture2D>("Graphics/Spark");
		}

		public override void Update(GameTime gameTime)
		{
			if (!isDead)
			{
				animate(gameTime);
				Move();

				// Check collisions with enemies
				for (int i = level.EnemyList.Count - 1; i >= 0; i--)
				{
					if (rect.Intersects(level.EnemyList[i].Rect) && level.EnemyList[i].Active && recoveryTimer <= 0 && !level.EnemyList[i].IsOnGround)
					{
						if (game.FiringMode.GetType() != typeof(NormalFire)) {
							try {
								level.EnemyList.RemoveAt(i);
							}
							catch (IndexOutOfRangeException) {

							}
						}
						takeHit();
					}
				}

				// Check collisions with wall tiles
				for (int i = level.TileList.Count - 1; i >= 0; i--)
				{
					if (rect.Intersects(level.TileList[i].Rect) && level.TileList[i].Solid)
					{
						Die();
					}
				}

				// Check collisions with bullets
				for (int i = level.BulletList.Count - 1; i >= 0; i--)
				{
					if (rect.Intersects(level.BulletList[i].Rect) && level.BulletList[i].Ship == BulletShip.Enemy && recoveryTimer <= 0)
					{
						if (level.BulletList[i].GetType() == typeof(Missle)) {
							Explosion.SpawnExplosion(level.EnemyList[i].Rect.X, level.EnemyList[i].Rect.Y, level);
							level.EnemyList.RemoveAt(i);
						}
						sparkRect = new Rectangle(level.BulletList[i].Rect.X, level.BulletList[i].Rect.Y, 16, 16);
						takeHit();
						break;
					}
				}

				// Check collisions with items
				for (int i = level.ItemList.Count - 1; i >= 0; i--)
				{
					if (rect.Intersects(level.ItemList[i].Rect))
					{
						level.ItemList[i].OnCollect();
					}
				}

				// Check if player reaches a checkpoint
				for (int i = level.CheckpointList.Count - 1; i >= 0; i--) {
					if (rect.Intersects(level.CheckpointList[i].GreaterRect)) {
						level.LastCheckPoint = level.CheckpointList[i];
					}
				}

				// Check if player reaches a warningpoint
				for (int i = level.WarningPointList.Count - 1; i >= 0; i--) {
					if (rect.Intersects(level.WarningPointList[i].Rect)) {
						level.WarningPointList.RemoveAt(i);
						level.Hud.SetCenterText("Enemy Ships Behind!");
					}
				}

				if (recoveryTimer > 0)
				{
					recoveryTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					blinkTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (blinkTimer <= 0)
					{
						blinkTimer = BLINKINTERVAL;
						invisible = !invisible;
					}
				}

				// If power up text timer is on, display the powerup text
				if (powerUpTextTimer > 0f)
				{
					powerUpTextTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				}

				if (machineGunTimer > 0)
				{
					machineGunTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				}
			}
		}

		/// <summary>
		/// Method called when hit by an enemy or bullet
		/// </summary>
		void takeHit()
		{
			DamageSound.Play();
			if (game.FiringMode.GetType() == typeof(NormalFire))
			{
				criticalHit.Play();
				level.StartDeathPause();
				//Die();
			}
			else
			{
				recoveryTimer = RECOVERYINTERVAL;
				//Globals.PowerUp = PowerUp.Normal;
				game.FiringMode = new NormalFire(this.game);
				DisplayPowerUpText("Power Down!");
			}
		}

		public void Move()
		{
			// Move horizontally and check for collisions
			rect.X += (int)(Velocity.X + level.Camera.Velocity.X);
			if (rect.X < 0)
			{
				rect.X = 0;
			}
			else if (rect.Right > Globals.SCREEN_WIDTH)
			{
				rect.X = Globals.SCREEN_WIDTH - rect.Width;
			}

			// Move vertically and check for collisions
			rect.Y += (int)(Velocity.Y + level.Camera.Velocity.Y);
			if (rect.Y < level.Camera.Rect.Top && level.State != LevelState.Completed)
			{
				rect.Y = level.Camera.Rect.Top;
			}
			else if (rect.Bottom > level.Camera.Rect.Bottom)
			{
				rect.Y = level.Camera.Rect.Bottom - rect.Height;
			}
		}

		/// <summary>
		/// Fires a bullet based on the powerup status.
		/// </summary>
		public void Shoot(KeyboardState prevState)
		{
			if (!isDead) // Check to see if the player is not dead
			{
				/*
				switch (Globals.PowerUp)
				{
					case PowerUp.Normal:
						ShootSound.Play();
						Bullet.SpawnBullet(rect.Center.X - 2, rect.Y, new Vector2(0, -12f), level);
						break;
					case PowerUp.MachineGun:
						if (MachineGun.State != SoundState.Playing)
						{
							MachineGun.Play();
							Bullet.SpawnBullet(rect.Center.X - 1, rect.Y, new Vector2(0, -12f), level);
						}
						break;
					case PowerUp.TwinBlaster:
						ShootSound.Play();
						Bullet.SpawnBullet(rect.X, rect.Y, new Vector2(0, -Globals.BULLET_SPEED), level);
						Bullet.SpawnBullet(rect.Right - 2, rect.Y, new Vector2(0, -Globals.BULLET_SPEED), level);
						break;
					case PowerUp.SideBlaster:
						ShootSound.Play();
						Bullet.SpawnSuperBullet(rect.Center.X - 6, rect.Y, new Vector2(0, -Globals.BULLET_SPEED), level);
						Bullet.SpawnSuperBullet(rect.X - 12, rect.Y, new Vector2(12f * -.259f, 12f * -.966f), level);
						Bullet.SpawnSuperBullet(rect.Right, rect.Y, new Vector2(12f * .259f, 12f * -.966f), level);
						break;
					case PowerUp.RearBlaster:
						ShootSound.Play();
						Bullet.SpawnBullet(rect.Center.X - 2, rect.Y, new Vector2(0, -12f), level);
						Bullet.SpawnBullet(rect.Center.X - 2, rect.Y + 9, new Vector2(0, 12f), level);
						break;
					case PowerUp.SuperBlaster:
						ShootSound.Play();
						Bullet.SpawnSuperBullet(rect.Center.X - 6, rect.Y, new Vector2(0, -12f), level);
						break;
				}
				*/
				game.FiringMode.Shoot(prevState);
			}
		}

		/// <summary>
		/// Bomb ground targets.
		/// </summary>
		public void Bomb()
		{
			BombSound.Play();
			level.Bomb = new Bomb(rect.Center.X - 6, rect.Top - 6, level);
		}

		/// <summary>
		/// Handle keyboard events
		/// </summary>
		/// <param name="keyState">Key state.</param>
		public void HandleEvents(KeyboardState keyState, KeyboardState prevState, GamePadState gamePadState, GamePadState prevPadState) {
			if (keyState.IsKeyDown(Keys.Up) || gamePadState.IsButtonDown(Buttons.DPadUp))
			{
				Velocity.Y = -4f;
			}
			else if (keyState.IsKeyDown(Keys.Down) || gamePadState.IsButtonDown(Buttons.DPadDown))
			{
				Velocity.Y = 4f;
			}
			
			if (keyState.IsKeyDown(Keys.Left) || gamePadState.IsButtonDown(Buttons.DPadLeft)) {
				Velocity.X = -4f;
			}
			else if (keyState.IsKeyDown(Keys.Right) || gamePadState.IsButtonDown(Buttons.DPadRight)) {
				Velocity.X = 4f;
			}

			if (keyState.IsKeyDown(Keys.X) || gamePadState.IsButtonDown(Buttons.A))
			{
				if ((!prevState.IsKeyDown(Keys.X) && !prevPadState.IsButtonDown(Buttons.A)) || game.FiringMode.GetType() == typeof(MachineGunFire))
				{
					Shoot(prevState);
				}
			}
			if ((keyState.IsKeyDown(Keys.Z) && !prevState.IsKeyDown(Keys.Z)) || (gamePadState.IsButtonDown(Buttons.B) && !prevPadState.IsButtonDown(Buttons.B)))
			{
				if (level.Bomb == null)
				{
					Bomb();
				}
			}

			/*
			// For debugging purposes ONLY! 
			//TODO: Comment out when release
			if (keyState.IsKeyDown(Keys.B) && !prevState.IsKeyDown(Keys.B))
			{
				level.StartDeathPause();
				//Die();
			}
			*/
		}

		void animate(GameTime gameTime) {
			animationTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (animationTimer <= 0) {
				animationTimer = ANIMATION_INTERVAL;
				animationIndex++;
				if (animationIndex >= MAX_FRAMES) {
					animationIndex = 0;
				}
				frame = animationFrames[animationIndex];
				
			}
		}

		/// <summary>
		/// Sets player to dead state.
		/// </summary>
		public void Die()
		{
			Explosion.SpawnExplosion(rect.X, rect.Y, level);
			isDead = true;
			Globals.Lives--;
			//Globals.PowerUp = PowerUp.Normal;
			game.FiringMode = new NormalFire(game);
			level.StartDeathTimer();
		}

		/// <summary>
		/// Resets the position to default after dying
		/// </summary>
		public void ResetPosition()
		{
			isDead = false; // Set isDead to false
			recoveryTimer = RECOVERYINTERVAL; // Start recovery timer

			//rect.X = (level.Camera.Rect.Width / 2) - (rect.Width / 2);
			rect.Y = level.Camera.Rect.Bottom - rect.Height;
			for (int i = level.TileList.Count - 1; i > 0; i--) {
				if (!level.TileList[i].Solid) {
					rect.X = level.TileList[i].Rect.X;
					break;
				}
			}
		}

		public void DisplayPowerUpText(string text)
		{
			powerUpText = text;
			powerUpTextTimer = powerUpTextInterval;
		}

		public void SetPosition(int x, int y) {
			rect.X = x;
			rect.Y = y;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (!isDead && (!invisible || recoveryTimer <= 0))
			{
				if (HasTrail) {
					spriteBatch.Draw(trail, new Rectangle(rect.X, rect.Bottom, trail.Width, rect.Bottom - TrailStart.Y), Color.White);
					spriteBatch.Draw(trail, new Rectangle(rect.Right - trail.Width, rect.Bottom, trail.Width, rect.Bottom - TrailStart.Y), Color.White);
				}

				// Draw the engine flames
				spriteBatch.Draw(texture, new Rectangle(rect.Center.X - 3, rect.Center.Y, 16, 16), frame, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);

				// Draw the ship based on the current power status
				/*
				switch (Globals.PowerUp)
				{
					case PowerUp.Normal:
					case PowerUp.SuperBlaster:
						spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, 32, 32), new Rectangle(1, 81, 16, 16), Color.Black, 0.0f, Vector2.Zero,
						SpriteEffects.None, 0.5f);
						spriteBatch.Draw(texture, rect, new Rectangle(1, 81, 16, 16), Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
						break;
					case PowerUp.MachineGun:
						spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, 32, 32), new Rectangle(1, 61, 16, 16), Color.Black, 0.0f, Vector2.Zero,
						SpriteEffects.None, 0.5f);
						spriteBatch.Draw(texture, rect, new Rectangle(1, 61, 16, 16), Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
						break;
					case PowerUp.TwinBlaster:
						spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, 32, 32), new Rectangle(1, 41, 16, 16), Color.Black, 0.0f, Vector2.Zero,
						SpriteEffects.None, 0.5f);
						spriteBatch.Draw(texture, rect, new Rectangle(1, 41, 16, 16), Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
						break;
					case PowerUp.SideBlaster:
						spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, 32, 32), new Rectangle(1, 21, 16, 16), Color.Black, 0.0f, Vector2.Zero,
						SpriteEffects.None, 0.5f);
						spriteBatch.Draw(texture, rect, new Rectangle(1, 21, 16, 16), Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
						break;
					case PowerUp.RearBlaster:
						spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, 32, 32), new Rectangle(1, 1, 16, 16), Color.Black, 0.0f, Vector2.Zero,
						SpriteEffects.None, 0.5f);
						spriteBatch.Draw(texture, rect, new Rectangle(1, 1, 16, 16), Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
						break;
				}
				*/
				spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, 32, 32), game.FiringMode.SourceRect, Color.Black, 0.0f, Vector2.Zero,
						SpriteEffects.None, 0.5f);
				spriteBatch.Draw(texture, rect, game.FiringMode.SourceRect, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
				if (powerUpTextTimer > 0)
				{
					//spriteBatch.DrawString(powerUpFont, powerUpText, new Vector2(rect.X, rect.Y - powerUpFont.MeasureString(powerUpText).Y), Color.White);
					bitFont.DrawString(powerUpText, new Rectangle(rect.X, rect.Y - 8, 8, 8), Color.White, spriteBatch);
				}
			}
			/*
			if (level.DeathPauseRunning && sparkRect != null) {
				spriteBatch.Draw(spark, sparkRect, Color.White);
			}
			*/
		}
	}
}