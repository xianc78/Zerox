﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
namespace Shooter
{
	public class Barrier : Enemy
	{
		bool dead;
		int length;

		SoundEffect deathSound;
		public Barrier(int x, int y, int length, Level level) : base(x, y, Shooter.DropItem.None, level)
		{
			hp = 4;
			this.length = length;
			rect.Width = this.length * Globals.TILE_SIZE;
			texture = game.Content.Load<Texture2D>("Graphics/Barrier");
			dead = false;
			deathSound = game.Content.Load<SoundEffect>("Sounds/Barrier");
		}

		public override void Update(GameTime gameTime)
		{
			if (!dead) {
				base.Update(gameTime);
			}
			else {
				if (rect.Width > 0) {
					rect.Width -= 8;
				}
				else {
					level.EnemyList.Remove(this);
				}
			}
		}

		public override void Die()
		{
			deathSound.Play();
			dead = true;
		}

		public override void Draw(SpriteBatch spriteBatch) {
			spriteBatch.Draw(texture, rect, null, Color.White);
		}
	}
}
