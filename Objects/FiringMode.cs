using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Shooter {
    public class FiringMode {
        protected Game1 game;
        protected Player player;
        protected Rectangle sourceRect;
        public Rectangle SourceRect {
            get {
                return sourceRect;
            }
        }
        protected SoundEffect shootSound;
        public FiringMode(Game1 game) {
            this.game = game;
            shootSound = game.Content.Load<SoundEffect>("Sounds/Bullet");
        }

        public virtual void Shoot(KeyboardState prevState) {

        }
    }

    public class NormalFire : FiringMode {
        public NormalFire(Game1 game) : base(game) {
            sourceRect = new Rectangle(1, 81, 16, 16);
        }
        public override void Shoot(KeyboardState prevState) 
        {
            if (!prevState.IsKeyDown(Keys.Z)) {
                shootSound.Play();
                Bullet.SpawnBullet(game.Level.Player.Rect.Center.X - 2, game.Level.Player.Rect.Y, new Vector2(0, -12f), game.Level);
            }
        }
    }

    public class MachineGunFire : FiringMode {
        public MachineGunFire(Game1 game) : base(game) {
            sourceRect = new Rectangle(1, 61, 16, 16);
        }
        public override void Shoot(KeyboardState prevState) {
            if (game.Level.Player.MachineGun.State != SoundState.Playing) {
                game.Level.Player.MachineGun.Play();
                Bullet.SpawnBullet(game.Level.Player.Rect.Center.X - 1, game.Level.Player.Rect.Y, new Vector2(0, -12f), game.Level);
            }
        }
    }

    public class TwinFire : FiringMode {
        public TwinFire(Game1 game) : base(game) {
            sourceRect = new Rectangle(1, 41, 16, 16);
        }
        public override void Shoot(KeyboardState prevState)
        {
            if (!prevState.IsKeyDown(Keys.Z)) {
                shootSound.Play();
                Bullet.SpawnBullet(game.Level.Player.Rect.X, game.Level.Player.Rect.Y, new Vector2(0, -Globals.BULLET_SPEED), game.Level);
				Bullet.SpawnBullet(game.Level.Player.Rect.Right - 2, game.Level.Player.Rect.Y, new Vector2(0, -Globals.BULLET_SPEED), game.Level);
            }
        }
    }

    public class RearFire : FiringMode {
        public RearFire(Game1 game) : base(game) {
            sourceRect = new Rectangle(1, 1, 16, 16);
        }
        public override void Shoot(KeyboardState prevState)
        {
            if (!prevState.IsKeyDown(Keys.Z)) {
                shootSound.Play();
				Bullet.SpawnBullet(game.Level.Player.Rect.Center.X - 2, game.Level.Player.Rect.Y, new Vector2(0, -12f), game.Level);
				Bullet.SpawnBullet(game.Level.Player.Rect.Center.X - 2, game.Level.Player.Rect.Y + 9, new Vector2(0, 12f), game.Level);
            }
        }
    }

    public class SprayFire : FiringMode {
        public SprayFire(Game1 game) : base(game) {
            sourceRect = new Rectangle(1, 21, 16, 16);
        }

        public override void Shoot(KeyboardState prevState)
        {
            shootSound.Play();
            Bullet.SpawnSuperBullet(game.Level.Player.Rect.Center.X - 6, game.Level.Player.Rect.Y, new Vector2(0, -Globals.BULLET_SPEED), game.Level);
			Bullet.SpawnSuperBullet(game.Level.Player.Rect.X - 12, game.Level.Player.Rect.Y, new Vector2(12f * -.259f, 12f * -.966f), game.Level);
			Bullet.SpawnSuperBullet(game.Level.Player.Rect.Right, game.Level.Player.Rect.Y, new Vector2(12f * .259f, 12f * -.966f), game.Level);
        }
    }
}