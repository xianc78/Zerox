﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	/// <summary>
	/// This boss alternates between bullets and missles
	/// </summary>
	public class Boss4 : Boss1
	{
		enum FiringMode { Bullets, Missles, SuperBullets, Lasers };
		FiringMode firingMode;

		int minX;
		int maxX;

		SoundEffect laserSound;


		double chargeTimer;
		const double CHARGE_INTERVAL = 700f;

		protected override int MAX_BULLETS
		{
			get
			{
				if (firingMode == FiringMode.Bullets)
				{
					return 30;
				}
				else if (firingMode == FiringMode.Lasers || firingMode == FiringMode.SuperBullets)
				{
					return 1;
				}
				return 7;
			}
		}

		double animationTimer;
		double ANIMATION_INTERVAL;
		Rectangle[] frames;
		int index;

		protected override double SHOOT_INTERVAL
		{
			get
			{
				if (firingMode == FiringMode.Missles)
				{
					return 750f;
				}
				else if (firingMode == FiringMode.Lasers) {
					return 300f;
				}
				return 75f;
			}
		}

		public override int MAX_HP {
			get {
				return 300;
			}
		}

		public Boss4(int x, int y, Level level) : base(x, y, level)
		{
			// Load assets
			texture = game.Content.Load<Texture2D>("Graphics/Boss1");
			laserSound = game.Content.Load<SoundEffect>("Sounds/Laser");

			firingMode = FiringMode.Bullets;
			minX = rect.X - 20;
			maxX = rect.Right + 20;
			chargeTimer = chargeSound.Duration.TotalMilliseconds;

			frames = new Rectangle[] {new Rectangle(3, 149, 122, 101), new Rectangle(137, 149, 122, 101), new Rectangle(271, 149, 122, 101), 
									  new Rectangle(405, 149, 122, 101), new Rectangle(539, 149, 122, 101), new Rectangle(673, 149, 122, 101)};
			ANIMATION_INTERVAL = chargeSound.Duration.TotalMilliseconds / frames.Length;
			animationTimer = ANIMATION_INTERVAL;
			index = 0;
		}

		protected override void attack(GameTime gameTime)
		{

			/*
			if (firingMode == FiringMode.Bullets)
			{
				if (rect.X < minX)
				{
					rect.X = minX;
					Velocity.X = -Velocity.X;
				}
				else if (rect.Right > maxX)
				{
					rect.X = maxX - rect.Width;
					Velocity.X = -Velocity.X;
				}
			}
			else
			{
				Velocity = Vector2.Zero;
			}
			*/
			Velocity = Vector2.Zero;

			if (chargeTimer >= 0) {
				chargeTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			}
			else {
				shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				if (shootTimer <= 0f)
				{
					shoot();
					bulletCount++;
					if (bulletCount >= MAX_BULLETS)
					{
						bulletCount = 0;
						state = BossState.Normal;
						Velocity.X = -4f;
						switch (firingMode)
						{
							case FiringMode.Bullets:
								firingMode = FiringMode.Missles;
								break;
							case FiringMode.Missles:
								firingMode = FiringMode.SuperBullets;
								break;
							case FiringMode.SuperBullets:
								firingMode = FiringMode.Lasers;
								break;
							case FiringMode.Lasers:
								minX = rect.X - 20;
								maxX = rect.Right + 20;
								firingMode = FiringMode.Bullets;
								break;
						}
						chargeTimer = chargeSound.Duration.TotalMilliseconds;
					}
					shootTimer = SHOOT_INTERVAL;
				}
			}
		}

		void animate(GameTime gameTime) {
			animationTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (animationTimer <= 0f) {
				animationTimer = ANIMATION_INTERVAL;
				index++;
				if (index >= frames.Length) {
					index = 0;
				}
			}
		}

		protected override void shoot()
		{
			switch (firingMode)
			{
				case FiringMode.SuperBullets:
					Bullet.SpawnSuperEnemyBullet(rect.Center.X - 9, rect.Center.Y - 9, GetBulletVector() * 2, level);
					break;
				case FiringMode.Bullets:
					machineGunSound.Play();
					Bullet.SpawnEnemyBullet(rect.X + 15, rect.Bottom, new Vector2(0, 12f), level);
					Bullet.SpawnEnemyBullet(rect.X + (rect.Width / 2) - 6, rect.Bottom, new Vector2(0, 12), level);
					Bullet.SpawnEnemyBullet(rect.X + 99, rect.Bottom, new Vector2(0, 12f), level);
					break;
				case FiringMode.Missles:
					level.EnemyList.Add(new Missle(rect.X + 16, rect.Bottom, level));
					level.EnemyList.Add(new Missle(rect.Center.X, rect.Bottom, level));
					level.EnemyList.Add(new Missle(rect.Right - 16, rect.Bottom, level));
					break;
				case FiringMode.Lasers:
					laserSound.Play();
					level.BulletList.Add(new Laser(rect.X + 31, rect.Y + 30, level));
					level.BulletList.Add(new Laser(rect.X + 84, rect.Y + 30, level));
					break;
			}
			/*
			if (bulletCount == MAX_BULLETS)
			{
				level.EnemyList.Add(new Missle(rect.X + 16, rect.Bottom, level));
				level.EnemyList.Add(new Missle(rect.X + (rect.Width / 2) - 16, rect.Bottom, level));
				level.EnemyList.Add(new Missle(rect.Right - 16, rect.Bottom, level));
			}
			*/
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (bossExplosion != null) {
				bossExplosion.Draw(spriteBatch);
			}
			else if (!BossExplosionDone){
				spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, rect.Width, rect.Height), new Rectangle(3, 149, 122, 101), Color.Black, 0.0f, 
								 Vector2.Zero, SpriteEffects.None, 0.1f);
				if (!hit) {
						spriteBatch.Draw(texture, rect, new Rectangle(3, 149, 122, 101), Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
						if (state == BossState.Attack && readyTimer >= 0) {
							spriteBatch.Draw(texture, rect, frames[index], Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
						}
				}
				else {
					spriteBatch.Draw(texture, rect, new Rectangle(3, 149, 122, 101), Color.Red, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
				}
			}
		}
	}
}
