﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace Shooter
{
	public class Boss : Enemy
	{
		protected enum BossState { Normal, Lunge, Attack, Charge, Slide, Explode, Dead }
		protected BossState state;

		protected double shootTimer;
		protected virtual double SHOOT_INTERVAL
		{
			get
			{
				return 75f;
			}
		}

		public virtual int MAX_HP
		{
			get
			{
				return 100;
			}
		}

		protected double attackTimer;
		protected const double ATTACK_INTERVAL = 5000f;

		protected double waitTimer;
		protected const double WAIT_INTERVAL = 2000f;

		protected double deathTimer;
		protected const double DEATH_INTERVAL = 3000f;

		protected double explosionTimer;
		protected const double EXPLOSION_INTERVAL = 300f;

		protected double readyTimer;
		protected const double READY_INTERVAL = 500f;

		protected double flashTimer;
		protected const double FLASH_INTERVAL = 200f;

		protected int explosionCount;
		protected const int MAX_EXPLOSIONS = 5;

		protected int bulletCount;
		protected virtual int MAX_BULLETS {
			get
			{
				return 30;
			}
		}

		protected string title;
		public string Title
		{
			get
			{
				return title;
			}
		}

		public virtual float SPEED {
			get {
				return 4f;
			}
		}

		protected Random rand;

		protected Texture2D flash;

		protected SoundEffect victorySound;

		protected SoundEffect criticalHit;

		/// <summary>
		/// Determines whether or not the boss fight has started
		/// </summary>
		//protected bool active;

		protected SoundEffect chargeSound;

		public BossExplosion bossExplosion;
		public bool BossExplosionDone;


		public Boss(int x, int y, Level level) : base(x, y, Shooter.DropItem.None, level)
		{
			rand = new Random();
			hp = MAX_HP;
			title = "Get ready!";
			chargeSound = game.Content.Load<SoundEffect>("Sounds/Charge");
			flash = game.Content.Load<Texture2D>("Graphics/Circle");
			victorySound = game.Content.Load<SoundEffect>("Sounds/Victory");
			criticalHit = game.Content.Load<SoundEffect>("Sounds/CriticalHit");
			active = false;
			deathTimer = DEATH_INTERVAL;
			BossExplosionDone = false;
		}

		public override void Die()
		{
			if (state != BossState.Explode && state != BossState.Dead) // make sure boss isn't already dead
			{
				criticalHit.Play();
				MediaPlayer.Stop();
				level.StartBossPause();
				hit = false;
				state = BossState.Explode;
				Velocity = level.Camera.Velocity;
			}
		}

		protected virtual void dead(GameTime gameTime) {
			if (bossExplosion == null) {
				Globals.Score += score;
				level.Boss = null;
				level.BossHealthBar.Visible = false;
				// End the level only if the player is still alive
				if (level.State != LevelState.Dead)
					//victorySound.Play();
					//level.End();
					level.StartBossDeathTimer();
			}
			else {
				bossExplosion.Update(gameTime);
			}
		}

		protected virtual void shoot()
		{

		}

		public override void Update(GameTime gameTime)
		{
			if (!active)
			{
				start();
			}
			base.Update(gameTime);
		}

		/// <summary>
		/// Stops camera and plays the boss BGM in preperation for the boss fight
		/// </summary>
		protected void start()
		{
			active = true;
			level.Camera.Stop();
			level.ShowBossHealthBar();
			level.Hud.SetCenterText(title);
			MediaPlayer.Play(level.bossBGM);

		}

		protected void explode(GameTime gameTime)
		{
			explosionTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (explosionTimer <= 0)
			{
				level.ExplosionList.Add(new Explosion(rand.Next(rect.Left, rect.Right), rand.Next(rect.Top, rect.Bottom), this.level));
				explosionTimer = EXPLOSION_INTERVAL;
				explosionCount++;
				if (explosionCount > MAX_EXPLOSIONS)
				{
					bossExplosion = new BossExplosion(rect.Center.X - 16, rect.Center.Y - 16, this, level);
					state = BossState.Dead;
					/*
					Globals.Score += score;
					level.Boss = null;
					level.BossHealthBar.Visible = false;
					// End the level only if the player is still alive
					if (level.State != LevelState.Dead)
						level.End();
					*/
				}
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (bossExplosion != null) {
				bossExplosion.Draw(spriteBatch);
			}
			else if (!BossExplosionDone) {
				spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, rect.Width, rect.Height), null, Color.Black, 0.0f, Vector2.Zero, 
					SpriteEffects.None, 0.1f);
				if (!hit) {
					spriteBatch.Draw(texture, rect, null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
				}
				else {
					spriteBatch.Draw(texture, rect, null, Color.Red, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
				}
			}
		}

		/*
		public override void Draw(SpriteBatch spriteBatch)
		{
			if (!hit)
			{
				base.Draw(spriteBatch);
			}
			else
			{
				spriteBatch.Draw(texture, destinationRectangle: rect, color: Color.Red);
			}
		}
		*/
	}
}
