using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class Boss5: Boss {
        double bulletAngle;
        
        const int MAX_SWIPES = 3;

        const double START_ANGLE = 3.93;
        const double END_ANGLE = 5.5;

        int swipeDirection;

        const int RIGHT = 0;
        const int LEFT = 1;

        public override int MAX_HP {
            get {
                return 200;
            }
        }

        protected override double SHOOT_INTERVAL {
            get {
                return 37f;
            }
        }

        SoundEffectInstance machineGunSound;

        int bounces;
        const int MAX_BOUNCES = 6;

        double chargeTimer;

        public Boss5(int x, int y, Level level) : base(x, y, level) {
            // Set bullet angle
            bulletAngle = START_ANGLE;

            // Set timers
            shootTimer = SHOOT_INTERVAL;
            attackTimer = ATTACK_INTERVAL;
            
            // Load texture
            texture = game.Content.Load<Texture2D>("Graphics/EnemyShip6");

            // Set rect size and velocity
            rect.Size = new Point(64, 64);
            Velocity = new Vector2(0f, 0f);

            // Set state
            state = BossState.Lunge;

            bounces = 0;
            swipeDirection = RIGHT;

            // Initialize sound effects
            machineGunSound = game.Content.Load<SoundEffect>("Sounds/MachineGun").CreateInstance();
            chargeTimer = chargeSound.Duration.TotalMilliseconds;

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            switch (state) {
                case BossState.Normal:
                    if ((rect.X <= Globals.SCREEN_WIDTH / 12) || (rect.X >= Globals.SCREEN_WIDTH - (Globals.SCREEN_WIDTH / 12) - rect.Width)) {
                        Velocity.X *= -1;
                        shoot();
                        bounces++;
                        if (bounces >= MAX_BOUNCES) {
                            state = BossState.Slide;
                            bounces = 0;
                        }
                    }
                    break;
                case BossState.Slide:
                    if (rect.Center.X == Globals.SCREEN_WIDTH / 2) {
                        state = BossState.Attack;
                        Velocity = Vector2.Zero;
                        chargeSound.Play();
                    }
                    else if (rect.Center.X > Globals.SCREEN_WIDTH / 2) {
                        Velocity.X = -4f;
                    }
                    else {
                        Velocity.X = 4f;
                    }
                    break;
                case BossState.Lunge:
                    if (rect.Y >= level.Camera.Rect.Y) {
                        state = BossState.Normal;
                        Velocity = new Vector2(-4f, 0f);
                    }
                    break;
                case BossState.Attack:
                    chargeTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (chargeTimer <= 0f) {
                        shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                        if (shootTimer <= 0f) {
                            shoot();
                            if (swipeDirection == RIGHT) {
                                bulletAngle += 0.08;
                                if (bulletAngle > END_ANGLE) {
                                    swipeDirection = LEFT;
                                }
                            }
                            else {
                                bulletAngle -= 0.08;
                                if (bulletAngle < START_ANGLE) {
                                    swipeDirection = RIGHT;
                                    state = BossState.Normal;
                                    Velocity.X = -4f;
                                    bulletAngle = START_ANGLE;
                                    chargeTimer = chargeSound.Duration.TotalMilliseconds;
                                }
                            }
                        shootTimer = SHOOT_INTERVAL;
                        }
                    }
                    break;
                case BossState.Explode:
                    explode(gameTime);
                    break;
                case BossState.Dead:
                    dead(gameTime);
                    break;
            }
            /*
            base.Update(gameTime);
            switch (state) {
                case BossState.Normal:
                    attackTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (attackTimer <= 0f) {
                        state = BossState.Slide;
                        attackTimer = ATTACK_INTERVAL;
                    }
                    break;
                case BossState.Attack:
                    shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (shootTimer <= 0f) {
                        shoot();
                        bulletAngle += 0.08;
                        if (bulletAngle > END_ANGLE) {
                            state = BossState.Slide;
                            bulletAngle = START_ANGLE;
                        }
                        shootTimer = SHOOT_INTERVAL;
                    }
                    
                    break;
                case BossState.Slide:
                    if (rect.X < destination) {
                        Velocity.X = 8f;
                    }
                    else if (rect.X > destination) {
                        Velocity.X = -8f;
                    }
                    else {
                        Velocity = Vector2.Zero;
                        state = BossState.Attack;
                        if (destination == Globals.SCREEN_WIDTH / 4) {
                            destination = Globals.SCREEN_WIDTH - (Globals.SCREEN_WIDTH / 4) - rect.Width;
                        }
                        else {
                            destination = Globals.SCREEN_WIDTH / 4;
                        }
                    }
                    break;
                case BossState.Dead:
                    explode(gameTime);
                    break;
            }
            */
        }
        protected override void shoot()
        {
            switch (state) {
                case BossState.Normal:
                    Bullet.SpawnEnemyBullet(rect.Left, rect.Bottom, new Vector2(0, 6f), level);
                    Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Bottom, new Vector2(0, 6f), level);
                    Bullet.SpawnEnemyBullet(rect.Right - 6, rect.Bottom, new Vector2(0, 6f), level);
                    break;
                case BossState.Attack:
                    if (machineGunSound.State != SoundState.Playing) {
                        machineGunSound.Play();
                    }
                    Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Bottom, new Vector2(6 * (float)Math.Cos(bulletAngle), -6 * (float)Math.Sin(bulletAngle)), level);
                    break;
            }
        }
    }
}