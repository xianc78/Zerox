﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Shooter
{
	/// <summary>
	/// Boss moves from left to right and shoots at certain intervals
	/// </summary>
	public class Boss1 : Boss
	{
		protected SoundEffectInstance machineGunSound;

		public override int MAX_HP
		{
			get
			{
				return 240;
			}
		}

		double animationTimer;
		double ANIMATION_INTERVAL;
		Rectangle[] frames;
		int index;

		public Boss1(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/Boss1");
			rect = new Rectangle(rect.X, rect.Y, 122, 101);
			state = BossState.Lunge;

			waitTimer = WAIT_INTERVAL;
			//deathTimer = DEATH_INTERVAL;
			explosionTimer = EXPLOSION_INTERVAL;
			shootTimer = SHOOT_INTERVAL;
			attackTimer = ATTACK_INTERVAL;
			readyTimer = chargeSound.Duration.TotalMilliseconds;

			bulletCount = 0;

			machineGunSound = game.Content.Load<SoundEffect>("Sounds/MachineGun").CreateInstance();

			Velocity = new Vector2(0, 2f);

			frames = new Rectangle[] {new Rectangle(3, 149, 122, 101), new Rectangle(137, 149, 122, 101), new Rectangle(271, 149, 122, 101), 
									  new Rectangle(405, 149, 122, 101), new Rectangle(539, 149, 122, 101), new Rectangle(673, 149, 122, 101)};
			ANIMATION_INTERVAL = chargeSound.Duration.TotalMilliseconds / frames.Length;
			animationTimer = ANIMATION_INTERVAL;
			index = 0;

		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			switch (state)
			{
				case BossState.Normal:
					if (rect.X < level.Camera.Rect.X)
					{
						rect.X = level.Camera.Rect.X;
						Velocity.X = -Velocity.X;
					}
					else if (rect.Right > level.Camera.Rect.Right) {
						rect.X = level.Camera.Rect.Right - rect.Width;
						Velocity.X = -Velocity.X;
					}
					attackTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (attackTimer <= 0)
					{
						state = BossState.Slide;
						attackTimer = rand.Next(3, 6) * 1000;
					}
					break;
				case BossState.Lunge:
					if (rect.Top >= level.Camera.Rect.Y)
					{
						Velocity.Y = 0;
						waitTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
						if (waitTimer <= 0)
						{
							Velocity = new Vector2(SPEED, 0f);
							state = BossState.Normal;
						}
					}
					break;
				case BossState.Slide:
					if (level.Player.Rect.X > rect.Right - (rect.Width / 4)) {
						Velocity.X = 6f;
					}
					else if (level.Player.Rect.Right < rect.X + (rect.Width / 4)) {
						Velocity.X = -6f;
					}
					else {
						chargeSound.Play();
						state = BossState.Attack;
					}
					break;
				case BossState.Attack:
					attack(gameTime);
					break;
				case BossState.Explode:
					explode(gameTime);
					break;
				case BossState.Dead:
					dead(gameTime);
					break;
			}
		}

		protected virtual void attack(GameTime gameTime)
		{
			// Stop the ship's horizontal velocity
			Velocity.X = 0;

			// Wait until the boss is ready to fire then shoot
			if (readyTimer >= 0f)
			{
				readyTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				animate(gameTime);
			}
			else
			{
				// Shoot every 100 milliseconds until bullet count reaches max bullets 
				shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				if (shootTimer <= 0f)
				{
					shoot();
					shootTimer = SHOOT_INTERVAL;
					bulletCount++;
					if (bulletCount > MAX_BULLETS)
					{
						bulletCount = 0;
						Velocity.X = -4f;
						readyTimer = chargeSound.Duration.TotalMilliseconds;
						state = BossState.Normal;
					}
					else if (bulletCount == MAX_BULLETS / 2)
					{
						shootDirectly();
					}
				}
			}
		}

		protected virtual void shootDirectly() {
			Bullet.SpawnSuperEnemyBullet(rect.Center.X - 9, rect.Center.Y - 9, GetBulletVector() * 2, level);
		}

		protected override void OnHit(Bullet bullet)
		{
			switch (state)
			{
				case BossState.Normal:
				case BossState.Attack:
					base.OnHit(bullet);
					break;
				case BossState.Lunge:
				case BossState.Dead:
					level.BulletList.Remove(bullet);
					break;
			}

		}

		protected override void move()
		{
			base.move();
		}

		protected override void shoot()
		{
			machineGunSound.Play();
			Bullet.SpawnEnemyBullet(rect.X + 15, rect.Bottom, new Vector2(0, 12f), level);
			Bullet.SpawnEnemyBullet(rect.X + (rect.Width / 2) - 6, rect.Bottom, new Vector2(0, 12), level);
			Bullet.SpawnEnemyBullet(rect.X + 99, rect.Bottom, new Vector2(0, 12f), level);
		}

		void animate(GameTime gameTime) {
			animationTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (animationTimer <= 0f) {
				animationTimer = ANIMATION_INTERVAL;
				index++;
				if (index >= frames.Length) {
					index = 0;
				}
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (bossExplosion != null) {
				bossExplosion.Draw(spriteBatch);
			}
			else {
				spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, rect.Width, rect.Height), new Rectangle(3, 149, 122, 101), Color.Black, 0.0f, 
								 Vector2.Zero, SpriteEffects.None, 0.1f);
				if (!hit) {
						spriteBatch.Draw(texture, rect, new Rectangle(3, 149, 122, 101), Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
						if (state == BossState.Attack && readyTimer >= 0) {
							spriteBatch.Draw(texture, rect, frames[index], Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
						}
				}
				else {
					spriteBatch.Draw(texture, rect, new Rectangle(3, 149, 122, 101), Color.Red, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
				}
			}
			
			//base.Draw(spriteBatch);
			/*
			if (readyTimer >= 0f && state == BossState.Attack) {
				spriteBatch.Draw(flash, new Vector2(rect.X + 11, rect.Y + rect.Height - 10), Color.White);
				spriteBatch.Draw(flash, new Vector2(rect.Right - 11, rect.Y + rect.Height - 10), Color.White);
			}
			*/
		}
	}
}
