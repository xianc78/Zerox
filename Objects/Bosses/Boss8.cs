using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class Boss8 : Boss {
        public List<ShieldGenerator> ShieldGeneratorList;
        bool IsShieldOn {
            get {
                return (ShieldGeneratorList.Count > 0);
            }
        }
        Rectangle shieldRect {
            get {
                return new Rectangle(rect.X - 32, rect.Y - 32, rect.Width + 64, rect.Height + 64);
            }
        }
        public override int MAX_HP {
            get {
                return 300;
            }
        }
        protected override double SHOOT_INTERVAL {
            get {
                return 4000f;
            }
        }
        Texture2D shieldTexture;
        double rechargeTimer;
        const double RECHARGE_INTERVAL = 5000f;
        SoundEffect deflectSound;

        public Boss8(int x, int y, Level level) : base(x, y, level) {
            texture = game.Content.Load<Texture2D>("Graphics/Boss");
            rect = new Rectangle(x, y, texture.Width, texture.Height);
            //shieldOn = false;
            ShieldGeneratorList = new List<ShieldGenerator>();
            rechargeTimer = RECHARGE_INTERVAL;
            regenerateShield();
            Velocity.X = 4f;
            state = BossState.Normal;
            deflectSound = game.Content.Load<SoundEffect>("Sounds/Deflect");
            shieldTexture = game.Content.Load<Texture2D>("Graphics/BossShield");
            shootTimer = SHOOT_INTERVAL;
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
            switch (state) {
                case BossState.Normal:
                    if (rect.X < level.Camera.Rect.X)
					{
						rect.X = level.Camera.Rect.X;
						Velocity.X = -Velocity.X;
					}
					else if (rect.Right > level.Camera.Rect.Right) {
						rect.X = level.Camera.Rect.Right - rect.Width;
						Velocity.X = -Velocity.X;
					}
                    shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (shootTimer <= 0) {
                        shoot();
                        shootTimer = SHOOT_INTERVAL;
                    }
                    break;
                case BossState.Explode:
                    explode(gameTime);
                    break;
                case BossState.Dead:
                    dead(gameTime);
                    break;
            }
            if (state != BossState.Dead && state != BossState.Explode) {
                if (IsShieldOn) {
                    for (int i = level.BulletList.Count - 1; i >= 0; i--) {
                        if (shieldRect.Intersects(level.BulletList[i].Rect) && level.BulletList[i].Ship == BulletShip.Player) {
                            level.BulletList.RemoveAt(i);
                            deflectSound.Play();
                        }
                    }    
                }
                else {
                        rechargeTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                        if (rechargeTimer <= 0) {
                            rechargeTimer = RECHARGE_INTERVAL;
                            regenerateShield();
                        }
                }
                for (int i = ShieldGeneratorList.Count - 1; i >= 0; i--) {
                    ShieldGeneratorList[i].Update(gameTime);
                }
            }            
        }


        void regenerateShield() {
            ShieldGeneratorList.Add(new ShieldGenerator(135, this));
            ShieldGeneratorList.Add(new ShieldGenerator(45, this));
            ShieldGeneratorList.Add(new ShieldGenerator(225, this));
            ShieldGeneratorList.Add(new ShieldGenerator(315, this));
        }

        protected override void shoot() {
            Vector2 vector = GetBulletVector();
            vector.X += 2f;
            vector.Y += 2f;
            Bullet.SpawnSuperEnemyBullet(rect.Center.X - 9, rect.Center.Y - 9, vector, level);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            base.Draw(spriteBatch);
            if (IsShieldOn) {
                spriteBatch.Draw(shieldTexture, new Rectangle(rect.Center.X - 102, rect.Center.Y - 102, 204, 204), Color.White);
            }
            for (int i = ShieldGeneratorList.Count - 1; i >= 0; i--) {
                ShieldGeneratorList[i].Draw(spriteBatch);
            }
        }
    }

    public class ShieldGenerator : GameObject {
        double angle;
        int hp;
        Boss8 boss;
        const int A = 154;
        const int B = 133;
        const int RADIUS = 100;
        bool hit;
        SoundEffect shutdownSound;

        public ShieldGenerator(double angle, Boss8 boss) : base(0, 0, boss.Level) {
            this.angle = angle;
            hp = 3;
            this.boss = boss;
            level = boss.Level;
            game = level.Game;
            texture = game.Content.Load<Texture2D>("Graphics/EnemyShip3");
            rect.Width = 32;
            rect.Height = 32;
            setPosition();
            shutdownSound = game.Content.Load<SoundEffect>("Sounds/Barrier");
        }

        void setPosition() {
            rect.X = (int)(Math.Cos(angle * (Math.PI/180)) * RADIUS + boss.Rect.Center.X - (rect.Width / 2));
            rect.Y = (int)(Math.Sin(angle * (Math.PI/180)) * RADIUS + boss.Rect.Center.Y - (rect.Height / 2));
        }

        public override void Update(GameTime gameTime) {
            hit = false;
            angle += 2;
            if (angle >= 360) {
                angle = angle % 360;
            }
            setPosition();

            for (int i = level.BulletList.Count - 1; i >= 0; i--) {
                if (rect.Intersects(level.BulletList[i].Rect) && level.BulletList[i].Ship == BulletShip.Player) {
                    hit = true;
                    hp--;
                    level.BulletList.RemoveAt(i);
                }
            }

            if (hp <= 0) {
                boss.ShieldGeneratorList.Remove(this);
                level.ExplosionList.Add(new Explosion(rect.X, rect.Y, level));
                if (boss.ShieldGeneratorList.Count == 0) {
                    shutdownSound.Play();
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch) {
            if (hit) {
                spriteBatch.Draw(texture, rect, null, Color.DarkRed, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
            }
            else {
                spriteBatch.Draw(texture, rect, null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 1.0f);
            }
            
        }
    }
}