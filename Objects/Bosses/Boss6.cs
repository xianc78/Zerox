using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class Boss6 : Boss {
        const int LEFT_BOUNDARY = Globals.SCREEN_WIDTH / 16;
        const int RIGHT_BOUNDARY = Globals.SCREEN_WIDTH - LEFT_BOUNDARY;
        //bool hasShot;
        int stage;
        SoundEffect shootSound;
        public Boss6(int x, int y, Level level) : base(x, y, level) {
            texture = game.Content.Load<Texture2D>("Graphics/Boss6");
            rect = new Rectangle(x, y, 96, 96);
            Velocity = new Vector2(-2f, 0f);
            //hasShot = false;
            stage = 0;
            shootSound = game.Content.Load<SoundEffect>("Sounds/Bomb");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            switch (state) {
                case BossState.Normal:
                /*
                    if (rect.X < LEFT_BOUNDARY) {
                        rect.X = LEFT_BOUNDARY;
                        Velocity.X *= -1;
                        shoot();
                        hasShot = false;
                    }
                    else if (rect.Right > RIGHT_BOUNDARY) {
                        rect.X = RIGHT_BOUNDARY - rect.Width;
                        Velocity.X *= -1;
                        shoot();
                        hasShot = false;
                    }
                    else if ((rect.Center.X >= Globals.SCREEN_WIDTH / 2 && Velocity.X < 0) || (rect.Center.X <= Globals.SCREEN_WIDTH / 2 && Velocity.X > 0)) {
                        if (!hasShot) {
                            shoot();
                            hasShot = true;
                        }                        
                    }
                    */
                    
                    // Depending on what stage in the boss's movement, check to see if it reaches the boundary and go to the next stage
                    switch (stage) {
                        case 0:
                            if (rect.Left <= 0) {
                                stage = 1;
                                Velocity = new Vector2(0f, 2f);
                                //shoot();
                            }
                            break;
                        case 1:
                            if (rect.Y >= Globals.TILE_SIZE * 2) {
                                stage = 2;
                                Velocity = new Vector2(2f, 0f);
                                shoot();
                            }
                            break;
                        case 2:
                            if (rect.Center.X >= Globals.SCREEN_WIDTH / 2) {
                                stage = 3;
                                Velocity = new Vector2(0f, -2f);
                                //shoot();
                            }
                            break;
                        case 3:
                            if (rect.Y <= 0) {
                                stage = 4;
                                Velocity = new Vector2(2f, 0f);
                                shoot();
                            }
                            break;
                        case 4:
                            if (rect.Right >= Globals.SCREEN_WIDTH) {
                                stage = 5;
                                Velocity = new Vector2(0f, 2f);
                                //shoot();
                            }
                            break;
                        case 5:
                            if (rect.Y >= Globals.TILE_SIZE * 2) {
                                stage = 6;
                                Velocity = new Vector2(-2f, 0f);
                                shoot();
                            }
                            break;
                        case 6:
                            if (rect.Center.X <= Globals.SCREEN_WIDTH / 2) {
                                stage = 7;
                                Velocity = new Vector2(0f, -2f);
                                //shoot();
                            }
                            break;
                        case 7:
                            if (rect.Y <= 0) {
                                stage = 0;
                                Velocity = new Vector2(-2f, 0f);
                                shoot();
                            }
                            break;
                    }
                    break;
                case BossState.Explode:
                    explode(gameTime);
                    break;
                case BossState.Dead:
                    dead(gameTime);
                    break;
            }
        }

        protected override void shoot() {
            /*
            Bullet.SpawnEnemyBullet(rect.X + 6, rect.Bottom, new Vector2(-0.5f, (float)Math.Sqrt(3)/2) * 6f, level);
            Bullet.SpawnEnemyBullet(rect.Center.X, rect.Bottom, new Vector2(0, 8f), level);
            Bullet.SpawnEnemyBullet(rect.Right - 6, rect.Bottom, new Vector2(0.5f, (float)Math.Sqrt(3)/2) * 6f, level);
            */
            /*
            level.BulletList.Add(new Boss6Bullet(rect.X + 6, rect.Bottom, 0, level));
            level.BulletList.Add(new Boss6Bullet(rect.Center.X, rect.Bottom, 1, level));
            level.BulletList.Add(new Boss6Bullet(rect.Right - 6, rect.Bottom, 2, level));
            */
            shootSound.Play();
            level.BulletList.Add(new SuperBossBullet(rect.Center.X - 24, rect.Bottom - 12, level));
        }
    }
}