using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    ///<summary>
    /// This enemy shoots in a circle when it's in the center.
    ///</summary>
    public class Boss7 : Boss {
        int stage;
        const float SPEED = 4f;

        protected override double SHOOT_INTERVAL {
            get {
                return 750;
            }
        }
        public Boss7(int x, int y, Level level) : base(x, y, level) {
            texture = game.Content.Load<Texture2D>("Graphics/EnemyShip3");
            Velocity = new Vector2(SPEED, 0f);
            stage = 0;
            rect = new Rectangle(x, y, 96, 96);
            shootTimer = SHOOT_INTERVAL;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            switch (state) {
                case BossState.Normal:
                    switch (stage) {
                        case 0:
                            if (rect.Right >= Globals.SCREEN_WIDTH) {
                                Velocity = new Vector2(0f, SPEED);
                                stage = 1;
                            }
                            break;
                        case 1:
                            if (rect.Bottom >= Globals.SCREEN_HEIGHT) {
                                Velocity = new Vector2(-SPEED, 0f);
                                stage = 2;
                            }
                            break;
                        case 2:
                            if (rect.Left <= 0) {
                                Velocity = new Vector2(0f, -SPEED);
                                stage = 3;
                            }
                            break;
                        case 3:
                            if (rect.Top <= 0) {
                                Velocity = new Vector2(SPEED, 0f);
                                stage = 0;
                            }
                        break;
                    }
                    shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                    if (shootTimer <= 0) {
                        shootTimer = SHOOT_INTERVAL;
                        shoot();
                    }
                    break;
                case BossState.Explode:
                    explode(gameTime);
                    break;
                case BossState.Dead:
                    dead(gameTime);
                    break;
            }
        }

        protected override void shoot()
        {
            Bullet.SpawnEnemyBullet(rect.Center.X, rect.Center.Y, GetBulletVector(), level);
        }
    }
}