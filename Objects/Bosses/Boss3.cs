﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{

	enum EyeBallState { Open, Closed, Squint }

	public class Boss3 : Boss
	{
		EyeBallState eyeState;
		double blinkTimer;
		const double BLINK_INTERVAL = 1000;
		Rectangle[] animationFrames = {new Rectangle(0, 11, 128, 80), new Rectangle(128, 11, 128, 80), new Rectangle(256, 11, 128, 80), 
									   new Rectangle(384, 11, 128, 80), new Rectangle(512, 11, 128, 80)};
		double animationTimer;
		const double ANIMATION_INTERVAL = 200;

		int animationIndex;

		protected override Rectangle hitBox
		{
			get
			{
				return new Rectangle(rect.X + 32, rect.Y, 128, 128);
			}
		}

		protected override double SHOOT_INTERVAL
		{
			get
			{
				return 100f;
			}
		}

		public override float SPEED {
			get {
				return 2f;
			}
		}

		SoundEffect deflectSound;
		SoundEffectInstance machineGunSound;

		public Boss3(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/BossShip3");
			rect = new Rectangle(x, y, 192, 128);
			deflectSound = game.Content.Load<SoundEffect>("Sounds/Deflect");
			machineGunSound = game.Content.Load<SoundEffect>("Sounds/MachineGun").CreateInstance();
			Velocity = new Vector2(-SPEED, 0f);
			state = BossState.Lunge;
			animationTimer = ANIMATION_INTERVAL;
			animationIndex = 0;
		}

		public override void Update(GameTime gameTime)
		{
			switch (state)
			{
				case BossState.Normal:
					base.Update(gameTime);

					if (rect.X < level.Camera.Rect.X)
					{
						rect.X = level.Camera.Rect.X;
						Velocity.X = -Velocity.X;
					}
					else if (rect.Right > level.Camera.Rect.Right)
					{
						rect.X = level.Camera.Rect.Right - rect.Width;
						Velocity.X = -Velocity.X;
					}

					// Update eyeball state
					blinkTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (blinkTimer <= 0f)
					{
						blinkTimer = BLINK_INTERVAL;
						switch (eyeState)
						{
							case EyeBallState.Open:
								eyeState = EyeBallState.Closed;
								break;
							case EyeBallState.Squint:
								eyeState = EyeBallState.Open;
								break;
							case EyeBallState.Closed:
								eyeState = EyeBallState.Squint;
								break;
								
						}
					}
					
					//animate(gameTime);

					// Update shoot timer if the eye is open
					if (eyeState == EyeBallState.Open)
					{
						shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
						if (shootTimer <= 0)
						{
							shoot();
							shootTimer = SHOOT_INTERVAL;
						}
					}

					break;
				case BossState.Lunge:
					if (rect.Top > level.Camera.Rect.Top)
					{
						level.Camera.Stop();
						state = BossState.Normal;
					}
					break;
				case BossState.Explode:
					explode(gameTime);
					break;
				case BossState.Dead:
					dead(gameTime);
					break;
			}
		}

		protected override void OnHit(Bullet bullet)
		{
			if (eyeState == EyeBallState.Open)
			{
				base.OnHit(bullet);
			}
			/*
			if (animationIndex == 0) {
				base.OnHit(bullet);
			}
			*/
			else
			{
				deflectSound.Play();
				level.BulletList.Remove(bullet);
			}
		}

		void animate(GameTime gameTime) {
			animationTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (animationTimer <= 0f) {
				animationTimer = ANIMATION_INTERVAL;
				animationIndex++;
				if (animationIndex >= animationFrames.Length) {
					animationIndex = 0;
				}
				//sourceRect = animationFrames[animationIndex];
			}
		}

		protected override void shoot()
		{
			if (machineGunSound.State != SoundState.Playing) {
				machineGunSound.Play();
			}
			Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Center.Y - 6, GetBulletVector(), level);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (bossExplosion != null) {
				bossExplosion.Draw(spriteBatch);
			}
			else if (!BossExplosionDone){
				Rectangle sourceRect = new Rectangle(0, 0, 64, 64);
				Color tint = Color.White;
				if (hit)
				{
					tint = Color.Red;
				}
				if (state != BossState.Explode)
				{
					switch (eyeState)
					{
						case EyeBallState.Open:
							sourceRect = new Rectangle(48, 0, 32, 32);
							break;
						case EyeBallState.Squint:
							sourceRect = new Rectangle(80, 0, 32, 32);
							break;
						case EyeBallState.Closed:
							sourceRect = new Rectangle(8, 0, 32, 32);
							break;
					}
				}
				else
				{
					sourceRect = new Rectangle(8, 0, 32, 32);
					//sourceRect = new Rectangle(128, 11, 128, 80);
					//sourceRect = new Rectangle(70, 0, 64, 64);
				}
				spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, rect.Width, rect.Height), new Rectangle(0, 0, 48, 32), Color.Black);
				spriteBatch.Draw(texture, rect, new Rectangle(0, 0, 48, 32), tint);
				spriteBatch.Draw(texture, new Rectangle(rect.X + 32, rect.Y, 128, 128), sourceRect, tint);
			}
		}
	}
}
