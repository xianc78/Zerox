﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	/// <summary>
	/// This boss shoots missles which chase the player
	/// </summary>
	public class Boss2 : Boss1
	{
		protected override double SHOOT_INTERVAL
		{
			get
			{
				return 1000f;
			}
		}

		protected override int MAX_BULLETS
		{
			get
			{
				return 7;
			}
		}

		public override int MAX_HP
		{
			get
			{
				return 200;
			}
		}

		public override float SPEED {
			get {
				return 2f;
			}
		}

		public Boss2(int x, int y, Level level) : base (x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/Boss2");
			rect = new Rectangle(x, y, texture.Width, texture.Height);
		}

		protected override void attack(GameTime gameTime)
		{
			base.attack(gameTime);
		}

		protected override void shoot()
		{
			level.EnemyList.Add(new Missle(rect.X + 16, rect.Bottom, level));
			level.EnemyList.Add(new Missle(rect.Center.X, rect.Bottom, level));
			level.EnemyList.Add(new Missle(rect.Right - 16, rect.Bottom, level));
		}

		protected void angledShoot()
		{

			machineGunSound.Play();

			// Calculate distance from player
			int distX = level.Player.Rect.Center.X - (rect.Center.X - 6);
			int distY = level.Player.Rect.Center.Y - rect.Bottom;

			// Calculate angle
			double angle = Math.Atan2(distY, distX);

			// Create the bullet
			Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Bottom - 6, new Vector2((float)Math.Cos(angle) * 12f, (float)Math.Sin(angle) * 12f), level);
		}

		protected override void shootDirectly() {
			// Do nothing (the direct shot makes it harder)
		}

		public override void Draw(SpriteBatch spriteBatch) {
			Color tint;
			if (hit) {
				tint = Color.Red;
			}
			else {
				tint = Color.White;
			}
			if (bossExplosion != null) {
				bossExplosion.Draw(spriteBatch);
			}
			else {
				spriteBatch.Draw(texture, new Rectangle(rect.X, rect.Y, 119, 136), new Rectangle(0, 0, 119, 136), tint);
				spriteBatch.Draw(texture, new Rectangle(rect.X + 119, rect.Y, 119, 136), new Rectangle(0, 0, 119, 136), tint, 0.0f, Vector2.Zero, 
				SpriteEffects.FlipHorizontally, 0.9f);
			}
		}
	}
}
