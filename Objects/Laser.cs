﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Laser : Bullet
	{
		public Laser(int x, int y, Level level) : base(x, y, new Vector2(0, 24f), BulletShip.Enemy, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/Laser");
			rect = new Rectangle(x, y, 6, 32);
		}

		public override void Update(GameTime gameTime)
		{
			if (rect.Height >= 128)
			{
				base.Update(gameTime);
			}
			else
			{
				rect.Height += 32;
			}
		}
	}
}