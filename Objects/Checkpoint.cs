using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class Checkpoint {
        Rectangle rect;
        public Rectangle Rect {
            get {
                return rect;
            }
        }
        public Rectangle GreaterRect {
            get {
               return new Rectangle(0, rect.Y, Globals.SCREEN_WIDTH, rect.Height);
            }
        }

        public Checkpoint(int x, int y, Level level) {
            rect = new Rectangle(x, y, 32, 32);
        }
    }
}