﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class GameObject
	{
		protected Texture2D texture;
		protected Level level;
		public Level Level
		{
			get
			{
				return level;
			}
		}

		protected Game1 game;
		protected Rectangle rect;
		public Rectangle Rect
		{
			get
			{
				return rect;
			}
		}

		public Vector2 Velocity;

		public GameObject(int x, int y, Level level)
		{
			this.level = level;
			this.game = level.Game;

		}

		public virtual void Update(GameTime gameTime)
		{
			
		}

		public virtual void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(texture, rect, Color.White);
		}
	}
}
