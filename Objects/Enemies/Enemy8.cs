﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Enemy8 : Enemy
	{
		int stage;
		int pattern;
		double rotation;
		int radius;
		int cx;
		int cy;
		double exitAngle;
		Point previousLocation;
		public Enemy8(int x, int y, Level level) : base(x, y, Shooter.DropItem.None, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip8");
			stage = 0;
			hp = 1;
			Velocity = new Vector2(0f, 2f);
			score = 100;
			
		}

		public override void Update(GameTime gameTime)
		{
			previousLocation = rect.Center;
			base.Update(gameTime);
			switch (stage)
			{
				case 0:
					if (rect.Y >= level.Camera.Rect.Y + Globals.TILE_SIZE * 4)
					{
						stage = 1;
						if (rect.Right <= level.Camera.Rect.Center.X)
						{
							pattern = 0;
							cx = rect.X + Globals.TILE_SIZE;
							radius = Math.Abs(rect.X - cx);
							exitAngle = -(Math.PI);
							rotation = Math.PI;
						}
						else
						{
							pattern = 1;
							cx = rect.X - Globals.TILE_SIZE;
							radius = Math.Abs(rect.X - cx);
							exitAngle = Math.PI * 2;
							rotation = -Math.PI * 2;
						}
						cy = rect.Y - level.Camera.Rect.Y;
					}
					break;
				case 1:
					Velocity = Vector2.Zero;
					if (pattern == 0)
					{
						rotation -= 0.02 * 8f;
					}
					else
					{
						rotation += 0.02 * 8f;
					}

					if ((rotation <= exitAngle && pattern == 0) || (rotation >= exitAngle && pattern == 1))
					{
						stage = 3;
						Velocity = new Vector2(0f, 2f);
					}
					rect.Location = new Point((int)(cx + Math.Cos(rotation) * radius), (int)(level.Camera.Rect.Y + Globals.TILE_SIZE * 4 + Math.Sin(rotation) * radius));
					break;
				case 2:
					break;
			}
		}

		float calcAngle() {
            int distX = rect.Center.X - previousLocation.X;
            int distY = rect.Center.Y - previousLocation.Y;

            double angle = Math.Atan2((double)distY, (double)distX);
            angle = Math.Floor(angle / (Math.PI / 4)) * (Math.PI / 4);

            return (float)angle;
        }

		public static void SpawnGroup(int x, int y, Level level)
		{
			level.EnemyList.Add(new Enemy8(x, y, level));
			level.EnemyList.Add(new Enemy8(x, y - Globals.TILE_SIZE, level));
			level.EnemyList.Add(new Enemy8(x, y - (Globals.TILE_SIZE * 2), level));
			level.EnemyList.Add(new Enemy8(x, y - (Globals.TILE_SIZE * 3), level));
			level.EnemyList.Add(new Enemy8(x, y - (Globals.TILE_SIZE * 4), level));
		}

		public override void Draw(SpriteBatch spriteBatch)
        {
            float angle = calcAngle();
            spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 32, rect.Y + 16, rect.Width, rect.Height), null, Color.Black, angle, 
                             new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
			if (hit)
			{
				spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 16, rect.Y + 16, rect.Width, rect.Height), null, Color.Red, angle,
                                 new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0f);
				}
			else
			{
				spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 16, rect.Y + 16, rect.Width, rect.Height), null, Color.White, angle,
                                 new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0f);
			}
        }
	}
}