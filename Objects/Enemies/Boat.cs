using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class Boat : Tank {
        public Boat(int x, int y, Direction facing, Level level) : base(x, y, facing, level) {
            texture = game.Content.Load<Texture2D>("Graphics/Boat");
        }
    }
}