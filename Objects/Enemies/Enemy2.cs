﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	/// <summary>
	/// Enemy lunges out at the player and then shoots
	/// </summary>
	public class Enemy2 : Enemy
	{

		enum EnemyState { Lunge, Attack, Flee };
		EnemyState state;

		double shootTimer;
		const double SHOOTINTERVAL = 500f;

		public Enemy2(int x, int y, DropItem dropItem, Level level) : base(x, y, dropItem, level)
		{
			hp = 2;
			score = 400;
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip2");
			Velocity = new Vector2(0, 4f);
			shootTimer = SHOOTINTERVAL;
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			switch (state)
			{
				case EnemyState.Lunge:
					
					if (rect.Y >= level.Camera.Rect.Center.Y - (rect.Height * 2))
					{
						state = EnemyState.Attack;
						Velocity = level.Camera.Velocity;
					}
					break;
				case EnemyState.Attack:
					shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (shootTimer <= 0f)
					{
						Shoot();
						state = EnemyState.Flee;
						Velocity = new Vector2(0, -8f);
					}
					break;
				case EnemyState.Flee:
					if (rect.Y < level.Camera.Rect.Y)
					{
						level.EnemyList.Remove(this);
					}
					break;
			}
		}

		public void Shoot()
		{
			Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Bottom, GetBulletVector(), level);
		}



		public override void Draw(SpriteBatch spriteBatch)
		{
			if (state == EnemyState.Flee)
			{
				spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y, 32, 32), null, Color.Black, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0f);
				if (!hit)
				{
					spriteBatch.Draw(texture, rect, null, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0f);
				}
				else
				{
					spriteBatch.Draw(texture, rect, null, Color.Red, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0f);
				}
			}
			else
			{
				base.Draw(spriteBatch);
			}
		}


	}
}