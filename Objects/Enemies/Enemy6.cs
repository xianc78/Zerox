﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	/// <summary>
	/// This enemy moves in a sine curve.
	/// </summary>
	public class Enemy6 : Enemy
	{
		int minX, maxX;
		const int WAVE_LENGTH = Globals.TILE_SIZE * 2;
		public Enemy6(int x, int y, DropItem dropItem, Level level) : base(x, y, dropItem, level)
		{
			minX = rect.X - (Globals.TILE_SIZE);
			maxX = rect.X + (Globals.TILE_SIZE);
			Velocity = new Vector2(-2f, 2f);
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip6");
			hp = 1;
			score = 100;
		}

		protected override void move()
		{
			base.move();
			if (rect.X < minX || rect.X > maxX)
			{
				Velocity.X *= -1;
			}
		}

		public static void SpawnGroup(int x, int y, Level level)
		{
			for (int i = 0; i < 5; i++)
			{
				level.EnemyList.Add(new Enemy6(x, y - (Globals.TILE_SIZE * i), Shooter.DropItem.None, level));
			}
		}
	}
}
