﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Shooter
{
	public class Missle : Enemy
	{
		/// <summary>
		/// Determines whether or not the missle is chasing the player.
		/// </summary>
		bool chase;

		double chaseTimer;
		const double CHASE_INTERVAL = 3000f;

		/// <summary>
		/// Angle the missle is rotated. Can be any angle divislbe by PI/4 radians
		/// </summary>
		double angle;

		SoundEffect missleSound;
		public Missle(int x, int y, Level level) : base(x, y, Shooter.DropItem.None, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/Missle");
			missleSound = game.Content.Load<SoundEffect>("Sounds/Missle");
			hp = 1;
			chase = true;
			calcAngle();

			chaseTimer = CHASE_INTERVAL;

			missleSound.Play();
		}

		public void calcAngle()
		{
			/*
			int distX = level.Player.Rect.Center.X - rect.Center.X;
			int distY = level.Player.Rect.Center.Y - rect.Center.Y;
			*/
			angle = Math.Floor(GetPlayerAngle() / (Math.PI / 4)) * (Math.PI / 4);
		}

		public override void Update(GameTime gameTime)
		{
			if (chase)
			{
				// Calculate the angle between itself and player
				calcAngle();

				// Calculate the velocity from the angle
				Velocity = new Vector2((float)Math.Cos(angle) * 5f, (float)Math.Sin(angle) * 5f);

				// If the player is dead or the missle is below the player, stop chasing
				if (level.Player.IsDead || level.Player.IsRecovering || rect.Y > level.Player.Rect.Bottom)
				{
					chase = false;
				}
				chaseTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
				if (chaseTimer <= 0)
				{
					chase = false;
				}
			}
			base.Update(gameTime);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			/*
			spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, 32, 32), null, Color.White, (float)angle, 
							new Vector2(texture.Width / 2, texture.Height / 2), Vector2.Zero, SpriteEffects.None, 0f);
			*/
			spriteBatch.Draw(texture, new Rectangle(rect.X + 32, rect.Y + 16, 32, 32), null, Color.Black,
							(float)angle, new Vector2(texture.Width / 2, texture.Height / 2), SpriteEffects.None,
							0f);
			spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, 32, 32), null, Color.White,
							(float)angle, new Vector2(texture.Width / 2, texture.Height / 2), SpriteEffects.None,
							0f);
		}
	}
}
