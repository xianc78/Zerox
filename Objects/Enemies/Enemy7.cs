﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{

	/// <summary>
	/// Enemy flees to the side when close to the player
	/// </summary>

	public class Enemy7 : Enemy
	{
		public Enemy7(int x, int y, Level level) : base(x, y, Shooter.DropItem.None, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip7");
			hp = 1;
			Velocity = new Vector2(0f, 2f);
			score = 100;
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			if (rect.Bottom > level.Player.Rect.Top - (Globals.TILE_SIZE * 4))
			{
				if (rect.Right <= Globals.SCREEN_WIDTH / 2)
				{
					Velocity.X = -4f;
				}
				else
				{
					Velocity.X = 4f;
				}
			}
		}
	}
}
