﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	/// <summary>
	/// This enemy is immune to bullets.
	/// </summary>
	public class Enemy5 : Enemy
	{
		SoundEffect deflectSound;
		public Enemy5(int x, int y, DropItem dropItem, Level level) : base(x, y, dropItem, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip5");
			hp = 1;
			deflectSound = game.Content.Load<SoundEffect>("Sounds/Deflect");
		}

		protected override void OnHit(Bullet bullet)
		{
			level.BulletList.Remove(bullet);
			deflectSound.Play();
		}
	}
}
