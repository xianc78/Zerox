﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Turret : Enemy
	{
		double facing;
		double shootTimer;
		const double SHOOT_INTERVAL = 2000f;

		double rotationTimer;
		const double rotationInterval = 200f;

		double destinationAngle;
		double prevAngle;

		double rotationSpeed;

		bool isInLineOfSight {
			get {
				double angle;
				int distX = level.Player.Rect.Center.X - rect.Center.X;
				int distY = level.Player.Rect.Center.Y - rect.Center.Y;

				angle = Math.Floor(Math.Atan2(distY, distX) / (Math.PI/4)) * (Math.PI / 4);

				if (facing == angle) {
					return true;
				}

				return false;
			}
		}
		public Turret(int x, int y, Level level) : base(x, y, Shooter.DropItem.None, level) 
		{
			texture = game.Content.Load<Texture2D>("Graphics/Turret");
			facing = (3 * Math.PI) / 2;
			prevAngle = facing;
			hp = 3;
			score = 300;
			isOnGround = true;
			rotationSpeed = 0;
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (shootTimer <= 0f && isInLineOfSight)
			{
				shoot();
				shootTimer = SHOOT_INTERVAL;
			}
			
			rotationTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (rotationTimer <= 0) {
				calcAngle();
				setRotation();
				facing += rotationSpeed;

				// If the rotation passes the destination angle, set it to the destination angle
				if ((prevAngle < destinationAngle && destinationAngle < facing) || (prevAngle > destinationAngle && destinationAngle > facing)) {
					facing = destinationAngle;
					rotationSpeed = 0;
				}
				
				prevAngle = facing;
				rotationTimer = rotationInterval;
			}
		}

		protected override void OnBombed(Bomb bomb)
		{
			base.OnBombed(bomb);
			Die();
		}

		protected override void OnHit(Bullet bullet)
		{
			
		}

		///<summary>
		/// Sets the rotation speed based on the distance between the two angles
		///</summary>
		void setRotation() {
			/*
			double deltaAngle;
			deltaAngle = Math.Atan2(Math.Sin(destinationAngle - facing), Math.Cos(destinationAngle - facing));
			if (deltaAngle < 0) {
				rotationSpeed = -Math.PI/4;
			}
			else if (deltaAngle > 0) {
				rotationSpeed = Math.PI/4;
			}
			else {
				rotationSpeed = 0;
			}
			*/
			double deltaAngle;
			double facingDegrees = facing * (180/Math.PI);
			double destinationDegrees = destinationAngle * (180/Math.PI);


			//deltaAngle = Math.Sin(facingDegrees - destinationDegrees);
			deltaAngle = (destinationDegrees - facingDegrees) % 360;


			if (deltaAngle > 0) {
				rotationSpeed = Math.PI/4;
			}
			else if (deltaAngle < 0){
				rotationSpeed = -Math.PI/4;
			}
			else {
				rotationSpeed = 0;
			}

		}

		///<summary>
		/// Calculate the angle between the player and the turret
		///</summary>
		void calcAngle()
		{
			double angle = GetPlayerAngle();
			destinationAngle = Math.Floor(angle / (Math.PI / 4)) * (Math.PI / 4);
		}

		float rotation()
		{
			return (float)facing;
			//return (float)Math.Floor(facing / ((float)Math.PI / 4)) * ((float)Math.PI / 4);
		}

		void shoot()
		{
			float angle = rotation();
			Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Center.Y - 6, new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * 6, level);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y  + 16, 32, 32), null, Color.White, rotation(), 
							new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
		}

	}
}
