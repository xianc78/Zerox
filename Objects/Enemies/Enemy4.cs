﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	/// <summary>
	/// This enemy hides from the left or right side of the screen and pops when in line of sight of the player
	/// </summary>
	public class Enemy4 : Enemy
	{
		enum EnemyState { Rest, Lunge }
		EnemyState state;
		bool rotated;
		double animationTimer;
		const double ANIMATION_INTERVAL = 100f;
		public Enemy4(int x, int y, Level level) : base(x, y, Shooter.DropItem.None, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip4");
			state = EnemyState.Rest;
			Velocity = Vector2.Zero;
			hp = 1;
			score = 100;

			rotated = false;
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			switch (state)
			{
				case EnemyState.Rest:
					if (rect.Bottom > level.Player.Rect.Y)
					{
						state = EnemyState.Lunge;
						if (rect.X > Globals.SCREEN_WIDTH / 2)
						{
							Velocity = new Vector2(-4f, level.Camera.Velocity.Y);
						}
						else
						{
							Velocity = new Vector2(4f, level.Camera.Velocity.Y);
						}
					}
					break;
				case EnemyState.Lunge:
					animationTimer += gameTime.ElapsedGameTime.TotalMilliseconds;
					if (animationTimer >= ANIMATION_INTERVAL) {
						animationTimer = 0f;
						if (rotated) {
							rotated = false;
						}
						else {
							rotated = true;
						}
					}
					break;
			}
		}

		public static void SpawnGroup(int x, int y, Level level)
		{
			level.EnemyList.Add(new Enemy4(x, y, level));
			level.EnemyList.Add(new Enemy4(x, y - (Globals.TILE_SIZE * 2), level));
			level.EnemyList.Add(new Enemy4(x - (Globals.TILE_SIZE * 2), y, level));
		}
		
		public override void Draw(SpriteBatch spriteBatch) {
			if (rotated) {
				spriteBatch.Draw(texture, new Rectangle(rect.X + 32, rect.Y + 16, 32, 32), null, Color.Black, 0.79f, new Vector2(texture.Width/2, texture.Height/2),
				SpriteEffects.None, 0.0f);
				spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, 32, 32), null, Color.White, 0.79f, new Vector2(texture.Width/2, texture.Height/2),
				SpriteEffects.None, 0.0f);
			}
			else {
				base.Draw(spriteBatch);
			}
		}
	}
}