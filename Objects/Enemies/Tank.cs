﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	/// <summary>
	/// Enemy is like the turret but moves
	/// </summary>
	public class Tank : Enemy
	{
		Direction facing;
		double turretFacing;

		double shootTimer;
		const double SHOOT_INTERVAL = 1500f;

		Texture2D turretTexture;
		public Tank(int x, int y, Direction facing, Level level) : base (x, y, Shooter.DropItem.None, level)
		{
			hp = 1;
			
			score = 500;
			this.facing = facing;
			texture = game.Content.Load<Texture2D>("Graphics/Tank");
			turretTexture = game.Content.Load<Texture2D>("Graphics/TankTurret");
			isOnGround = true;
			shootTimer = SHOOT_INTERVAL;

			switch (this.facing) {
				case Direction.Up:
					turretFacing = Math.PI / 2;
					Velocity = new Vector2(0f, -1f);
					break;
				case Direction.Down:
					turretFacing = (3 * Math.PI) / 2;
					Velocity = new Vector2(0f, 1f);
					break;
				case Direction.Left:
					turretFacing = Math.PI;
					Velocity = new Vector2(-1f, 0f);
					break;
				case Direction.Right:
					turretFacing = 2 * Math.PI;
					Velocity = new Vector2(1f, 0f);
					break;
			}
		}

		void calcAngle()
		{
			double angle;
			int distX = level.Player.Rect.Center.X - rect.Center.X;
			int distY = level.Player.Rect.Center.Y - rect.Center.Y;

			angle = Math.Atan2(distY, distX);
			turretFacing = Math.Floor((angle / (Math.PI / 4))) * (Math.PI / 4);
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			shootTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (shootTimer <= 0) {
				shoot();
				shootTimer = SHOOT_INTERVAL;
			}
			calcAngle();
		}

		protected override void OnBombed(Bomb bomb)
		{
			base.OnBombed(bomb);
			hp--;
		}

		protected override void OnHit(Bullet bullet)
		{
			
		}

		protected void shoot() {
			float angle = (float)turretFacing;
			Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Center.Y - 6, new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * 6, level);
		}

		public override void Draw(SpriteBatch spriteBatch) {
			switch (facing) {
				case Direction.Up:
					spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, rect.Width, rect.Height), null, Color.White, 
					(float)-Math.PI/2, new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
					break;
				case Direction.Down:
					spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, rect.Width, rect.Height), null, Color.White, 
					(float)(-3 * Math.PI)/2, new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
					break;
				case Direction.Left:
					spriteBatch.Draw(texture, rect, null, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipHorizontally, 0.1f);
					break;
				case Direction.Right:
					spriteBatch.Draw(texture, rect, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.1f);
					break;
			}
			spriteBatch.Draw(turretTexture, new Rectangle(rect.X + 16, rect.Y + 16, turretTexture.Width * 2, turretTexture.Height * 2), null, 
			Color.White, (float)turretFacing, new Vector2(turretTexture.Width/2, turretTexture.Height/2), SpriteEffects.None, 0.1f);
		}
	}
}
