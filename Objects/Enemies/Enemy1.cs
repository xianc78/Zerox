﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Enemy1 : Enemy
	{
		public Enemy1(int x, int y, DropItem dropItem, Level level) : base(x, y, dropItem, level)
		{
			hp = 1;
			score = 200;
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip1");
			Velocity = new Vector2(0f, 2f);
		}

	}
}