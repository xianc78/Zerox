﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace Shooter
{
	/// <summary>
	/// This enemy heads directly towards the player.
	/// </summary>
	public class Enemy3 : Enemy
	{
		enum EnemyState { Lunge, Rest, Chase };
		EnemyState state;
		double restTimer;
		const double REST_INTERVAL = 500;
		public Enemy3(int x, int y, DropItem dropItem, Level level) : base(x, y, dropItem, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/EnemyShip3");
			state = EnemyState.Lunge;
			restTimer = REST_INTERVAL;
			hp = 2;
			score = 500;
			Velocity = new Vector2(0, 4f);
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			switch (state)
			{
				case EnemyState.Lunge:
					if (rect.Bottom >= level.Camera.Rect.Center.Y)
					{
						state = EnemyState.Rest;
						Velocity = level.Camera.Velocity;
					}
					break;
				case EnemyState.Rest:
					restTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
					if (restTimer <= 0)
					{
						Chase();
						state = EnemyState.Chase;
					}
					break;
				case EnemyState.Chase:
					break;
			}
		}

		void Chase()
		{
			int distX = level.Player.Rect.Center.X - rect.Center.X;
			int distY = level.Player.Rect.Center.Y - rect.Center.Y;

			double angle = Math.Atan2(distY, distX);

			Velocity.X = (float)Math.Cos(angle) * 6f;
			Velocity.Y = (float)Math.Sin(angle) * 6f;
		}
	}
}
