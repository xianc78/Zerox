﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Shooter
{
	public class Enemy : GameObject
	{
		protected int hp;
		public int Hp
		{
			get
			{
				return hp;
			}
		}

		protected int score;
		public int Score
		{
			get
			{
				return score;
			}
		}

		protected bool active;
		public bool Active {
			get 
			{
				return active;
			}
		}

		/// <summary>
		/// Determines if enemy got hit.
		/// </summary>
		protected bool hit = false;

		/// <summary>
		/// Sound played when damaged
		/// </summary>
		SoundEffect damageSound;

		DropItem dropItem;

		protected bool isOnGround;
		public bool IsOnGround
		{
			get
			{
				return isOnGround;
			}
		}

		protected virtual Rectangle hitBox
		{
			get
			{
				return rect;
			}
		}


		public Enemy(int x, int y, DropItem dropItem, Level level) : base(x, y, level)
		{
			rect = new Rectangle(x, y, Globals.TILE_SIZE, Globals.TILE_SIZE);
			this.dropItem = dropItem;
			damageSound = game.Content.Load<SoundEffect>("Sounds/Damage");
			active = true;
		}

		public override void Update(GameTime gameTime)
		{
			hit = false;
			move();
			for (int i = Level.BulletList.Count - 1; i >= 0; i--)
			{
				if (hitBox.Intersects(level.BulletList[i].Rect) && level.BulletList[i].Ship != BulletShip.Enemy)
				{
					OnHit(level.BulletList[i]);
				}
			}
			if (level.Bomb != null)
			{
				if (rect.Intersects(level.Bomb.Rect) && level.Bomb.Life == 0)
				{
					OnBombed(level.Bomb);
				}
			}
			if (hp <= 0)
			{
				Die();
			}
		}

		protected virtual void move()
		{
			rect.X += (int)Velocity.X;
			rect.Y += (int)Velocity.Y;
		}

		protected void DropItem()
		{
			switch (dropItem)
			{
				case Shooter.DropItem.None:
					break;
				case Shooter.DropItem.TwinBlaster:
					level.ItemList.Add(new TwinBlaster(rect.X, rect.Y, level));
					break;
				case Shooter.DropItem.OneUp:
					level.ItemList.Add(new OneUp(rect.X, rect.Y, level));
					break;
				case Shooter.DropItem.MachineGun:
					level.ItemList.Add(new MachineGun(rect.X, rect.Y, level));
					break;
				case Shooter.DropItem.SideBlaster:
					level.ItemList.Add(new SideBlaster(rect.X, rect.Y, level));
					break;
				case Shooter.DropItem.RearBlaster:
					level.ItemList.Add(new RearBlaster(rect.X, rect.Y, level));
					break;
			}
		}

		/// <summary>
		/// Method called when hit with a bomb
		/// </summary>
		/// <param name="bomb">Bomb.</param>
		protected virtual void OnBombed(Bomb bomb)
		{
			level.Bomb = null;
		}

		/// <summary>
		/// Method called when hit with a bullet
		/// </summary>
		/// <param name="bullet">Bullet.</param>
		protected virtual void OnHit(Bullet bullet)
		{
			if (active) {
				hp -= bullet.Damage; // take damage
				Level.BulletList.Remove(bullet); // remove bullet
				hit = true;
				if (hp > 0)
				{
					damageSound.Play();
				}
			}
		}

		protected double GetPlayerAngle() {
			int distX = level.Player.Rect.Center.X - rect.Center.X;
			int distY = level.Player.Rect.Center.Y - rect.Center.Y;

			double angle = Math.Atan2(distY, distX);

			return angle;
		}

		protected Vector2 GetBulletVector()
		{
			double angle = GetPlayerAngle();

			return new Vector2((float)Math.Cos(angle) * 6f, (float)Math.Sin(angle) * 6f);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (active) {
				spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 16, rect.Y, rect.Width, rect.Height), null, Color.Black, 0.0f, Vector2.Zero,
				SpriteEffects.None, 0.1f);
				if (hit)
				{
					spriteBatch.Draw(texture, destinationRectangle: rect, color: Color.Red);
				}
				else
				{
					base.Draw(spriteBatch);
				}
			}
		}

		public virtual void Die()
		{
			Explosion.SpawnExplosion(rect.X, rect.Y, level);
			Globals.Score += score;
			DropItem();
			if (score > 0) {
				level.ScoreTextList.Add(new ScoreText(rect.X, rect.Y - 10, level, score.ToString()));
			}
			level.EnemyList.Remove(this);
		}
	}
}