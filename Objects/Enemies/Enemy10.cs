using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    ///<summary>
    /// This enemy appears from behind
    ///</summary>
    public class Enemy10 : Enemy {
        public Enemy10(int x, int y, DropItem dropItem, Level level) : base(x, y, dropItem, level) {
            hp = 1;
            score = 300;
            active = false;
            texture = game.Content.Load<Texture2D>("Graphics/EnemyShip10");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (rect.Y > level.Camera.Rect.Bottom) {
                active = true;
                Velocity = new Vector2(0f, -8f);
            }
            if (rect.Bottom < level.Camera.Rect.Top) {
                level.EnemyList.Remove(this);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (active) {
                spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 16, rect.Y, rect.Width, rect.Height), null, Color.Black, 0.0f, Vector2.Zero,
				                 SpriteEffects.None, 0.1f);
                if (hit) {
                    spriteBatch.Draw(texture, rect, Color.Red);
                }
                else {
                    spriteBatch.Draw(texture, rect, Color.White);
                }
            }
        }
    }
}