using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    ///<summary>
    /// Enemy moves in a Bezier curve
    ///</summary>
    public class Enemy9 : Enemy {
        ///<summary>
        /// Time step
        ///</summary>
        float t;

        // Points for the Bezier curve
        Vector2 pointA;
        Vector2 pointB;
        Vector2 pointC;

        int side;

        Point previousLocation;

        bool isInLineOfSight {
            get 
            {
                return (rect.Center.X > level.Player.Rect.X && rect.Center.X < level.Player.Rect.Right);
            }
        }
        public Enemy9(int x, int y, float t, DropItem dropItem, int side, Level level) : base(x, y, dropItem, level) {
            texture = game.Content.Load<Texture2D>("Graphics/EnemyShip9");
            hp = 1;
            score = 300;
            this.t = t;

            this.side = side;

            // Set Bezier curve points
            if (side == 0) {
                pointA = new Vector2(0f, 0f);
                pointB = new Vector2(240f, 720f);
                pointC = new Vector2(288f, 0f);
            }
            else {
                pointA = new Vector2(288f, 0f);
                pointB = new Vector2(240f, 720f);
                pointC = new Vector2(32f, 0f);
            }
        }

        public override void Update(GameTime gameTime)
        {
            previousLocation = rect.Center;
            base.Update(gameTime);
            calculatePattern();
            /*
            if (isInLineOfSight && !hasShot) {
                shoot();
            }
            */
            
        }

        public void calculatePattern() {
            t += 0.0075f;
            Vector2 AtoB = Vector2.Lerp(pointA, pointB, t);
            Vector2 BtoC = Vector2.Lerp(pointB, pointC, t);
            Vector2 final = Vector2.Lerp(AtoB, BtoC, t);
            rect.X = (int)final.X;
            rect.Y = level.Camera.Rect.Y + (int)final.Y;
        }

        float calcAngle() {
            int distX = rect.Center.X - previousLocation.X;
            int distY = rect.Center.Y - previousLocation.Y;

            double angle = Math.Atan2((double)distY, (double)distX);
            angle = Math.Floor(angle / (Math.PI / 4)) * (Math.PI / 4);

            return (float)angle;
        }

        public static void SpawnGroup(int y, int side, Level level) {
            level.EnemyList.Add(new Enemy9(0, y, 0f, Shooter.DropItem.None, side, level));
            level.EnemyList.Add(new Enemy9(0, y - 32, 0f, Shooter.DropItem.None, side, level));
            level.EnemyList.Add(new Enemy9(0, y - 64, 0f, Shooter.DropItem.None, side, level));
            level.EnemyList.Add(new Enemy9(0, y - 96, 0f, Shooter.DropItem.None, side, level));
            level.EnemyList.Add(new Enemy9(0, y - 128, 0f, Shooter.DropItem.None, side, level));
        }

        /*
        void shoot() {
            hasShot = true;
            Bullet.SpawnEnemyBullet(rect.Center.X - 6, rect.Bottom, new Vector2(0, 8f), level);
        }
        */

        public override void Draw(SpriteBatch spriteBatch)
        {
            float angle = calcAngle();
            spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 32, rect.Y + 16, rect.Width, rect.Height), null, Color.Black, angle, 
                             new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
			if (hit)
			{
				spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 16, rect.Y + 16, rect.Width, rect.Height), null, Color.Red, angle,
                                 new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0f);
				}
			else
			{
				spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 16, rect.Y + 16, rect.Width, rect.Height), null, Color.White, angle,
                                 new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0f);
			}
        }
    }
}
