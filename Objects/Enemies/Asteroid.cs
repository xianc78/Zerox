﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Asteroid : Enemy
	{
		int animationIndex;
		double animationTimer;
		const double ANIMATION_INTERVAL = 200;
		public Asteroid(int x, int y, DropItem dropItem, Level level) : base(x, y, dropItem, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/Asteroid");
			hp = 4;
			score = 500;
			animationTimer = ANIMATION_INTERVAL;
			animationIndex = 0;
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			animationTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
			if (animationTimer <= 0) {
				animationIndex++;
				animationTimer = ANIMATION_INTERVAL;
				if (animationIndex >= 4) {
					animationIndex = 0;
				}
			}
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			switch (animationIndex) {
				case 0:
					spriteBatch.Draw(texture, rect, Color.White);
					break;
				case 1:
					spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, 32, 32), null, Color.White, 
					(float)(Math.PI/2), new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
					break;
				case 2:
					spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, 32, 32), null, Color.White, 
					(float)Math.PI, new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
					break;
				case 3:
					spriteBatch.Draw(texture, new Rectangle(rect.X + 16, rect.Y + 16, 32, 32), null, Color.White,
					(float)(3 * Math.PI)/2, new Vector2(texture.Width/2, texture.Height/2), SpriteEffects.None, 0.1f);
					break;
			}
			
		}
	}
}
