﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Tile : GameObject
	{
		bool solid;
		public bool Solid
		{
			get
			{
				return solid;
			}
		}
		Rectangle sourceRect;

		bool animated;
		double animationTimer;
		const double ANIMATION_INTERVAL = 200;
		int animationIndex;
		Rectangle[] frames;
		int MAX_FRAMES 
		{
			get
			{
				return frames.Length;
			}
		}
		public Tile(int x, int y, bool solid, Rectangle[] frames, Texture2D tileSet, Level level) : base(x, y, level)
		{
			texture = tileSet;
			this.solid = solid;
			rect = new Rectangle(x, y, Globals.TILE_SIZE, Globals.TILE_SIZE);
			this.frames = frames;
			this.sourceRect = frames[0];

			if (MAX_FRAMES > 1) {
				animated = true;
			}
			else {
				animated = false;
			}
		}

		public override void Update(GameTime gameTime) {
			// If tile is animated, animate the tile
			if (animated) {
				animationTimer -= gameTime.ElapsedGameTime.TotalMilliseconds; // Increment the animation timer
				// If the animation timer is greater than the animation interval, reset the timer and increment the animation index
				if (animationTimer <= 0) {
					animationTimer = ANIMATION_INTERVAL;
					animationIndex++;
					// If animation index is greater than the maximum amount of frames, go back to zero
					if (animationIndex >= MAX_FRAMES) {
						animationIndex = 0;
					}
					// Set the source rectangle to the current frame
					sourceRect = frames[animationIndex];
				}
			}
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(texture, rect, sourceRect, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.0f);
		}
	}
}
