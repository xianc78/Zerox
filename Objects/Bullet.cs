﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Bullet : GameObject
	{
		/// <summary>
		/// Determines if bullet was fired by the player or enemy
		/// </summary>
		BulletShip ship;
		public BulletShip Ship
		{
			get
			{
				return ship;
			}
		}

		protected int damage;
		public int Damage
		{
			get
			{
				return damage;
			}
		}

		public Bullet(int x, int y, Vector2 velocity, BulletShip ship, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/Bullet");
			Velocity = velocity;
			rect = new Rectangle(x, y, 12, 12);
			this.ship = ship;
			damage = 1;
		}

		public override void Update(GameTime gameTime)
		{
			rect.X += (int)Velocity.X;
			if (rect.Right < level.Camera.Rect.Left || rect.Left > level.Camera.Rect.Right)
			{
				level.BulletList.Remove(this);
			}
			rect.Y += (int)Velocity.Y;
			if (rect.Bottom < level.Camera.Rect.Top || rect.Top > level.Camera.Rect.Bottom)
			{
				level.BulletList.Remove(this);
			}

			for (int i = level.TileList.Count - 1; i >= 0; i--)
			{
				if (rect.Intersects(level.TileList[i].Rect) && level.TileList[i].Solid)
				{
					level.BulletList.Remove(this);
				}
			}
		}

		public Vector2 GetVector()
		{
			int distX = level.Player.Rect.Center.X - rect.Center.X;
			int distY = level.Player.Rect.Center.Y - rect.Center.Y;

			double angle = Math.Atan2(distY, distX);

			return new Vector2((float)Math.Cos(angle) * 6f, (float)Math.Sin(angle) * 6f);
		}

		public static void SpawnBullet(int x, int y, Vector2 velocity, Level level)
		{
			level.BulletList.Add(new Level1Bullet(x, y, velocity, level));
		}

		public static void SpawnSuperBullet(int x, int y, Vector2 velocity, Level level)
		{
			level.BulletList.Add(new SuperBullet(x, y, velocity, level));
		}

		public static void SpawnEnemyBullet(int x, int y, Vector2 velocity, Level level)
		{
			level.BulletList.Add(new Bullet(x, y, velocity, BulletShip.Enemy, level));
		}

		public static void SpawnSuperEnemyBullet(int x, int y, Vector2 velocity, Level level)
		{
			level.BulletList.Add(new SuperEnemyBullet(x, y, velocity, level));
		}
	}

	public class Level1Bullet : Bullet {
		public Level1Bullet(int x, int y, Vector2 velocity, Level level) : base(x, y, velocity, BulletShip.Player, level) {
			texture = game.Content.Load<Texture2D>("Graphics/Level1Bullet");
			rect = new Rectangle(rect.X, rect.Y, texture.Width, texture.Height);
			damage = 1;
		}
	}

	public class SuperBullet : Bullet
	{
		public SuperBullet(int x, int y, Vector2 velocity, Level level) : base(x, y, velocity, BulletShip.Player, level)
		{
			damage = 2;
		}
	}

	public class EnemyBullet : Bullet
	{
		public EnemyBullet(int x, int y, Vector2 velocity, Level level) : base(x, y, velocity, BulletShip.Enemy, level)
		{
			Velocity = velocity;
		}
	}

	public class SuperEnemyBullet : EnemyBullet
	{
		public SuperEnemyBullet(int x, int y, Vector2 velocity, Level level) : base(x, y, velocity, level)
		{
			rect.Size = new Point(18, 18);
		}

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
		}
	}

	public class MachineGunBullet : Bullet
	{
		public MachineGunBullet(int x, int y, Vector2 velocity, Level level) : base(x, y, velocity, BulletShip.Player, level)
		{
			damage = 1;
		}
	}

	public class SuperBossBullet : Bullet {
		int destination;
		public SuperBossBullet(int x, int y, Level level) : base(x, y, new Vector2(0, 4f), BulletShip.Enemy, level) {
			texture = game.Content.Load<Texture2D>("Graphics/SuperBullet");
			rect = new Rectangle(x, y, 48, 48);
			destination = rect.Bottom + (Globals.TILE_SIZE * 2);
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);
			if (rect.Y >= destination) {
				level.BulletList.Remove(this);
				level.BulletList.Add(new Boss6Bullet(rect.X, rect.Center.Y, 0, level));
				level.BulletList.Add(new Boss6Bullet(rect.Center.X, rect.Center.Y, 1, level));
				level.BulletList.Add(new Boss6Bullet(rect.Right, rect.Center.Y, 2, level));
			}
		}

		public override void Draw(SpriteBatch spriteBatch) {
			spriteBatch.Draw(texture, destinationRectangle: new Rectangle(rect.X + 16, rect.Y, rect.Width, rect.Height), null, Color.Black, 0.0f, Vector2.Zero,
				SpriteEffects.None, 0.1f);
			base.Draw(spriteBatch);
		}
	}

	public class Boss6Bullet : Bullet {
		int pattern;
		int startLocation;
		public Boss6Bullet(int x, int y, int pattern, Level level) : base(x, y, Vector2.Zero, BulletShip.Enemy, level) {
			texture = game.Content.Load<Texture2D>("Graphics/BossBullet");
			this.pattern = pattern;
			startLocation = x;
			rect = new Rectangle(x, y, texture.Width, texture.Height);
			
			switch (this.pattern) {
				case 0:
					Velocity = new Vector2(-6f, 0f);
					break;
				case 1:
					Velocity = new Vector2(0, 8f);
					break;
				case 2:
					Velocity = new Vector2(6f, 0f);
					break;
			}
		}

		public override void Update(GameTime gameTime) {
			base.Update(gameTime);

			if ((pattern == 0 && rect.X < startLocation - (Globals.TILE_SIZE * 2)) || (pattern == 2 && rect.X > startLocation + (Globals.TILE_SIZE * 2))) {
				Velocity = new Vector2(0, 8f);
			}
		}
	}
}