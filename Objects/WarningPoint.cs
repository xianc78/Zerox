using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class WarningPoint {
        Rectangle rect;
        public Rectangle Rect {
            get {
                return rect;
            }
        }
        public WarningPoint(int y) {
            this.rect = new Rectangle(0, y, Globals.SCREEN_WIDTH, 32);
        }
    }
}