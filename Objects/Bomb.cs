﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{

	/// <summary>
	/// This bullet has a limited life an can only destroy ground targets
	/// </summary>
	public class Bomb : Bullet
	{
		int life;
		const int LIFE_INTERVAL = Globals.TILE_SIZE * 3;

		public int Life
		{
			get
			{
				return life;
			}
		}

		public Bomb(int x, int y, Level level) : base(x, y, new Vector2(0, -8f), BulletShip.Player, level)
		{
			life = LIFE_INTERVAL;
		}

		public override void Update(GameTime gameTime)
		{
			rect.Y += (int)Velocity.Y;
			life -= (int)Math.Abs(Velocity.Y);
			if (life < 0)
			{
				level.Bomb = null;
			}
		}
	}
}
