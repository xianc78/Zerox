using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter {
    public class BossExplosion : Explosion {
        Boss boss;
        public BossExplosion(int x, int y, Boss boss, Level level) : base(x, y, level) {
            this.boss = boss;
            texture = game.Content.Load<Texture2D>("Graphics/BossExplosion");
        }

        public override void Update(GameTime gameTime)
        {
            rect.Inflate(4, 4);
            if (rect.Width > Globals.SCREEN_WIDTH) {
                boss.BossExplosionDone = true;
                boss.bossExplosion = null;
            }
        }
        
        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(texture, rect, null, Color.White);
        }
    }
}