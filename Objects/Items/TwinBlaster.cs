﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class TwinBlaster : Item
	{
		public TwinBlaster(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/TwinBlaster");
			text = "Twin Blaster";
		}

		public override void OnCollect()
		{
			base.OnCollect();
			level.Game.FiringMode = new TwinFire(level.Game);
			//Globals.PowerUp = PowerUp.TwinBlaster;
		}
	}
}