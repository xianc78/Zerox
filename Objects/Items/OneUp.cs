﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class OneUp : Item
	{
		public OneUp(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/OneUp");
			text = "1 Up";
		}

		public override void OnCollect()
		{
			base.OnCollect();
			Globals.Lives++;
		}
	}
}