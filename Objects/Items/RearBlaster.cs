﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class RearBlaster : Item
	{
		public RearBlaster(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/RearBlaster");
			text = "Rear Blaster";
		}

		public override void OnCollect()
		{
			base.OnCollect();
			game.FiringMode = new RearFire(level.Game);
		}
	}
}