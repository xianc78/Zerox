﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class MachineGun : Item
	{
		public MachineGun(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/MachineGun");
			text = "Machine Gun";
		}

		public override void OnCollect()
		{
			base.OnCollect();
			//Globals.PowerUp = PowerUp.MachineGun;
			level.Game.FiringMode = new MachineGunFire(level.Game);
		}
	}
}