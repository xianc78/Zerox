﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Item : GameObject
	{
		SoundEffect pickUpSound;
		protected String text;
		public Item(int x, int y, Level level) : base(x, y, level)
		{
			rect = new Rectangle(x, y, Globals.TILE_SIZE, Globals.TILE_SIZE);
			pickUpSound = game.Content.Load<SoundEffect>("Sounds/PickUp");
		}

		public virtual void OnCollect()
		{
			pickUpSound.Play();
			level.Player.DisplayPowerUpText(text);
			level.ItemList.Remove(this);
		}
	}
}