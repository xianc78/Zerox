﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class SideBlaster : Item
	{
		public SideBlaster(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/SideBlaster");
			text = "Spray Blaster";
		}

		public override void OnCollect()
		{
			base.OnCollect();
			game.FiringMode = new SprayFire(level.Game);
		}

	}
}
