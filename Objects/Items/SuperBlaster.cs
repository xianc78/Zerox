﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class SuperBlaster : Item
	{
		public SuperBlaster(int x, int y, Level level) : base(x, y, level)
		{
			texture = game.Content.Load<Texture2D>("Graphics/OneUp");
			text = "Super Blaster";
		}

		public override void OnCollect()
		{
			base.OnCollect();
		}
	}
}