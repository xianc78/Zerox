﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shooter
{
	public class Wall : Tile
	{
		public Wall(int x, int y, Texture2D tileSet, Level level) : base(x, y, true, new Rectangle[] { new Rectangle(21, 20, 16, 16) }, tileSet, level)
		{
			rect = new Rectangle(x, y, Globals.TILE_SIZE, Globals.TILE_SIZE);
		}
	}
}
